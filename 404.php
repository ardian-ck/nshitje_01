<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<section class="post-content container_404">
    <div class="row">
        <div class="container">
            <div class="span12 leftZero" id="main">
		<p>#404</p>
	        <span>Faqja që keni keni kërkuar nuk ekziston ! </span><br />
	         <a href="<?php bloginfo('url'); ?>" class="btn btn-primary"> Kthehu mbrapa </a> 
            </div>
	</div>
      </div>
</section>

<?php get_footer(); ?>