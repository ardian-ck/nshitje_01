<?php session_start(); ?>
<?php
/*
Template Name: Kerko automjete
 */
?>
<?php get_header(); ?>
<?php
  global $post;
  $list = array();
  $item = array();
  if(get_query_var('paged')) $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  if(get_query_var('page')) $paged = (get_query_var('page')) ? get_query_var('page') : 1;
  //var_dump($paged);
  if($_GET) {
    //var_dump($_GET);
    foreach($_GET as $key => $value) {
      stripslashes($key);
      htmlentities($value);
      if($value != '') {
        if($key != 'select_min' && $key != 'select_max' && $key != 'select_serie' && $key != 'select_year' && $key != 'select_modeli') {
        $item['taxonomy'] = htmlspecialchars($key);
        $item['terms'] = htmlspecialchars($value);
        $item['field'] = 'slug';
        $list[] = $item;
        }
      }
    }
    $_SESSION['car-search'] = !empty($list) ? $list : array();
  }

  $search = !empty($_SESSION['car-search']) ? $_SESSION['car-search'] : array();
  $cleanArray = array_merge(array('relation' => 'AND'), $search);
  $args['post_status'] = 'publish';
  $args['post_type'] = 'automjete';
  $args['order'] = 'ASC';
  $args['orderby'] = 'meta_value_num';
  $args['meta_key'] = 'ac_cmimi_auto';
  $args['showposts'] = 32;
  $args['paged'] = $paged;
  $args['tax_query'] = $cleanArray;
  $args['meta_query'] = array(
      'relation' => 'AND',);
  //nese kerkohet per cmim
  if( $_GET['select_min'] !== '' || $_GET['select_max'] !== '') {
    (esc_attr($_GET['select_min']) !== '') ? $min_price = intval($_GET['select_min']) : $min_price = 0;
    (esc_attr($_GET['select_max']) !== '') ? $max_price = intval($_GET['select_max']) : $max_price = 999999;

      array_push($args['meta_query'], array(
        'key' => 'ac_cmimi_auto',
        'value' => $min_price,
        'compare' => '>=',
        'type' => 'numeric'
      ),
      array(
        'key' => 'ac_cmimi_auto',
        'value' => $max_price,
        'compare' => '<=',
        'type' => 'numeric'
      ));
      }
      //nese kerkohet per tip -> model -> vitin e prodhimit 
      if(isset($_GET['select_serie']) || isset($_GET['select_modeli']) || isset($_GET['select_year']) ) {
        if($_GET['select_modeli'] != '') {
          array_push($args['meta_query'], array(
            'key' => 'tipi',
            'value' => esc_attr($_GET['select_modeli']),
            'compare' => '=',
            'type' => 'char'
          ));
        }

        if($_GET['select_serie'] != '') {
          array_push($args['meta_query'], array(
            'key' => 'modeli',
            'value' => esc_attr($_GET['select_serie']),
            'compare' => '=',
            'type' => 'char'
          ));
        }

        if($_GET['select_year'] != '') {
          array_push($args['meta_query'], array(
            'key' => 'ac_viti_prodhimit',
            'value' => esc_attr($_GET['select_year']),
            'compare' => '=',
            'type' => 'numeric'
          ));
        }
      }

  //var_dump($args);
  $wp_query = null;
  $wp_query = new WP_Query($args);
  $total_pages = $wp_query->max_num_pages;

?>
<script type="text/javascript">
//<![CDATA[
function imgLoaded(img){  
    $(img).parent().addClass('loaded');
  }
//]]>
</script>
<section class="post-content-special">
<div class="row">
  <div class="container">
    <div class="span12 leftZero">
    <?php
      if($wp_query->found_posts > 0 && $wp_query->found_posts != 1) {
        echo '<h3 class="found_posts"> Janë gjetur '.$wp_query->found_posts.' automjete.</h3>';
      }
      elseif($wp_query->found_posts == 1) {
        echo '<h3 class="found_posts">Është gjetur '.$wp_query->found_posts.' automjet.</h3>';
      }
      else {
        $ac_home = home_url();
        echo '<h3 class="found_posts" style="text-align:center"> Nuk ka asnjë automjet me këto parametra </h3>';
        //header("refresh:3;url=".$ac_home);
      }
      //echo ($the_query->found_posts > 0) ? '<h3 class="found_posts">'.$the_query->found_posts.' vetura.</h3>' : '<h3 class="found_posts"> Nuk ka asnjë veturë me këto parametra </h3>'; 
    ?>
    <div class="wrapper wf">
      <!-- BEGIN CONTROLS -->
      <nav class="controls just">
        <div class="group" id="Sorts">
          <div class="button active" id="ToList"><i></i>Listë</div>
          <div class="button" id="ToGrid"><i></i>Rrjetë</div>
        </div>
        <div class="group btn btn-danger" id="Filters">
          <div class="drop_down wf">
            <span class="anim150">Lloji i mjetit</span>
            <ul class="anim250">
              <li class="active" data-filter="all" data-dimension="region">Të gjitha</li>
              <?php
              $args = array('taxonomy' => 'lloji', 'hide_empty' => false);
              $terms = get_terms('lloji', $args);
              // var_dump($terms);
              foreach($terms as $term) {
              ?>
              <li data-filter="<?php echo strtolower($term->name); ?>" data-dimension="region"><?php echo ucfirst($term->name); ?></li>
               <?php
              }
              ?>
            </ul>
          </nav><!-- END CONTROLS -->     
      <!-- BEGIN PARKS -->
      <ul id="Parks" class="just leftZero">
        <!-- "TABLE" HEADER CONTAINING SORT BUTTONS (HIDDEN IN GRID MODE)-->
        <div class="list_header">
          <div class="meta name active desc" id="SortByName">
            Rendit shpalljet sipas alfabetit &nbsp;
            <span class="sort anim150 asc active" data-sort="data-name" data-order="desc"></span>
            <span class="sort anim150 desc" data-sort="data-name" data-order="asc"></span>  
          </div>
          <div class="meta region">Lloji</div>
          <div class="meta area" id="SortByArea">
            Rendit sipas çmimit &nbsp;
            <span class="sort anim150 asc" data-sort="data-area" data-order="asc"></span>
            <span class="sort anim150 desc" data-sort="data-area" data-order="desc"></span>
          </div>
        </div>
        
        <!-- FAIL ELEMENT -->
        
        <div class="fail_element anim250">Nuk ka asnjë shpallje me kriteret që keni kërkuar !</div>
        
        <?php
            if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post();
            ?>
            <?php 
              $auto_price = get_post_meta($post->ID, 'ac_cmimi_auto', true);
              $cmimi_ndryshuar = get_post_meta($post->ID,'ac_cmimi_ndryshuar_auto', true);
              $auto_location = get_post_meta($post->ID, 'lokacioni', true);
              $cmimi_test = get_post_meta($post->ID, 'ac_cmimi_test', true);
              $auto_location = get_post_meta($post->ID, 'lokacioni', true);
              if(is_numeric($auto_location)) {
                $lok = get_term_by('id', $auto_location, 'lokacioni', 'ARRAY_A');
                $auto_location = $lok['name'];
              }
              $auto_status = get_post_meta($post->ID, 'ac_cmimi_test', true);
              $auto_year = get_post_meta($post->ID, 'ac_viti_prodhimit', true);
              $kudos = get_kudos_count($post->ID);
              $status = array();
              if(isset($auto_status)&& $auto_status != '') {
	              foreach($auto_status as $s) {
	                  switch($s) {
	                      case "0":
	                      $status[] = "Me marrëveshje";
	                      break;
	
	                      case "1":
	                      $status[] = "Çmimi fiks";
	                      break;
	
	
	                      case "2":
	                      $status[] = "Ndërroj";
	                  }
	              }
              }
            ?>

        <!-- BEGIN LIST OF PARKS (MANY OF THESE ELEMENTS ARE VISIBLE ONLY IN LIST MODE)-->
        <?php 
        $terms_list = get_the_term_list($post->ID, 'lloji', '', '', '');

        ?>
        <li class="mix <?php echo strtolower(strip_tags($terms_list)); ?>" data-name="<?php the_title(); ?>" data-area="<?php echo $auto_price; ?>">
          <div class="meta name">
            <div class="img_wrapper">
              <?php
                //$url = wp_get_attachment_thumb_url(get_post_thumbnail_id($post->ID), 'medium');
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium');
                $url = $thumb[0];
              ?>
              <a href="<?php the_permalink(); ?>"><div class="img_wrapper"><img src="<?php echo $url; ?>" onload="imgLoaded(this)"/></div></a>
            </div>
            <div class="titles">
              <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
              <p><em><?php echo ac_limit_excerpt(); ?></em></p>
              <div class="main-info">
              <ul class="leftZero">
                <li><i class="icon icon-eye-open"></i><?php echo wpp_get_views($post->ID);?></li>
                <li><i class="icon icon-thumbs-up"></i><?php echo $kudos; ?></li>
                <li><i class="icon icon-map-marker"></i><?php echo ucfirst($auto_location); ?></li>
                <li><i class="icon icon-time"></i></i><?php the_time('H:i:s')?> | <?php echo get_the_date( 'd-m-Y' ); ?></li>
              </ul>
            </div>
            </div>
          </div>
          <div class="meta region">
            <p><?php echo get_the_term_list($post->ID, 'lloji', '','',''); ?></p>
          </div>
          <div class="meta area">
            <div>

              <?php
                if($auto_price < $cmimi_ndryshuar) {
                  $percent_change = ($cmimi_ndryshuar - $auto_price) / 100;
                  $difference_change = $cmimi_ndryshuar - $auto_price;

              ?>

              <p class="price_tag"><?php echo $auto_price; ?>&euro; <small><?php echo $cmimi_ndryshuar; ?>&euro;</small></p>
              <span class="badge badge-important">- <?php echo $difference_change; //round($percent_change, 2); ?>&euro;</span>
              <?php
                }
                else {
              ?>
              <p><?php echo $auto_price; ?>&euro;</p>
              <?php } ?>
            </div>
          </div>

          <div class="meta grid_price_status">
                <?php 
                if(isset($status)) {
                    foreach($status as $price_status){
                        echo 
                        '<span class="label label-inverse">'.$price_status.'</span>&nbsp;';
                   }
                }
                ?>
          </div>
          
          <div class="meta extra-info">
              <span class="year_info more_info"><i class="icon icon-wrench"></i><?php echo $auto_year; ?></span>
              <span class="location_info more_info"><i class="icon icon-map-marker"></i> <?php echo ucfirst($auto_location); ?> </span>
          </div>
        </li>
        
        <?php 
      endwhile;
        ?>
        
        <?php
        else:
        ?>
      <p>Nuk ka asnje post.</p>
      <?php
      endif;
      wp_reset_query();
      ?>
      </ul>
    </div> <!-- END DEMO WRAPPER -->
    <?php 
    if(function_exists("ac_pagination")) {
      ac_pagination($total_pages);
    } 
  ?>
</div><!--/span12 -->
</div><!-- /container -->
</div><!-- /row -->
</section><!-- /post-content -->
<?php get_footer(); ?>