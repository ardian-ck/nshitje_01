<?php get_header(); ?>
<?php 
//pjesa e kodit qe merr klikimet per secilin post.
ac_set_post_views(get_the_ID());
?>
<section class="single_post_automjete">
  <div class="container">
    <div id="primary" class="span12 leftZero content-area">
      <div class="span7 leftZero site-content" id="content" role="main">

      <?php /* The loop */ global $post; ?>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php 
        $args = array(
          'post_type' => 'attachment',
          'post_mime_type' => 'image',
          'numberposts' => null,
          'post_status' => null,
          'post_parent' => $post->ID
        );

        $attachments = get_posts($args);
        $attachments_array[$post->ID] = $attachments;
          
        $cmimi_ndryshuar = get_post_meta($post->ID,'ac_cmimi_ndryshuar_auto', true);
        $lloji_automjetit = get_post_meta($post->ID, 'lloji', true); //id 
        if(is_numeric($lloji_automjetit)) {
          $lloji = get_term_by('id', $lloji_automjetit, 'lloji', 'ARRAY_A');
          $lloji_automjetit = $lloji['name'];
        }
        

        $tipi_automjetit = get_post_meta($post->ID, 'tipi', true);
        $modeli_automjetit = get_post_meta($post->ID, 'modeli', true);
        $viti_prodhimit = get_post_meta($post->ID, 'ac_viti_prodhimit', true);
        //te dhenat e pergjithshme.
        $lenda_djegese = get_post_meta($post->ID, 'lenda_djegese', true);
        if(is_numeric($lenda_djegese)) {
          $lenda = get_term_by('id', $lenda_djegese, 'lenda_djegese', 'ARRAY_A');
          $lenda_djegese = $lenda['name'];
        }
        

        $tipi_transmisioni = get_post_meta($post->ID, 'transmisioni', true);
        if(is_numeric($tipi_transmisioni)) {
          $transmisioni = get_term_by('id', $tipi_transmisioni, 'transmisioni', 'ARRAY_A');
          $tipi_transmisioni = $transmisioni['name'];
        }
        

        $kubikazhi = get_post_meta($post->ID, 'ac_kubikazhi', true);
        $lokacioni = get_post_meta($post->ID, 'lokacioni', true);
                if(is_numeric($lokacioni)) {
                $lok = get_term_by('id', $lokacioni, 'lokacioni', 'ARRAY_A');
                $lokacioni = $lok['name'];
                }
        $kilometra = get_post_meta($post->ID, 'ac_km_kaluara', true);
        $cmimi_auto = get_post_meta($post->ID, 'ac_cmimi_auto', true);
        $cmimi_test = get_post_meta($post->ID, 'ac_cmimi_test', true);
        
        
        //te dhenat per kontakt
        $emri_postuesit = get_post_meta($post->ID, 'ac_emri_postuesit', true);
        $email_postuesit = get_post_meta($post->ID, 'ac_email_postuesit', true);
        $numri_kontaktues = get_post_meta($post->ID, 'ac_nr_postuesit', true);

        $modeli = get_the_term_list(get_the_ID(), 'modeli', "");
        $seria = get_post_meta($post->ID, 'modeli', true);
        $lokacioni = get_the_term_list(get_the_ID(), 'lokacioni', "");
      ?>
  
    

    <article id="post-<?php the_ID(); ?>" <?php post_class(); if(function_exists('live_edit')) { live_edit('post_title'); }  ?> data-postid="<?php echo get_the_ID(); ?>">
      <div class="single-post-header">
        <h3 id="post_title"><p><?php echo the_title(); ?></p></h3>
        <span class="price_badge"><?php echo $cmimi_auto; ?> <em>&euro;</em></span>
        <?php
          if($auto_price < $cmimi_ndryshuar) {
            $percent_change = ($cmimi_ndryshuar - $auto_price) / 100;
            $difference_change = $cmimi_ndryshuar - $auto_price;
        ?>
        <span class="price-reduced badge badge-important"><?php echo $cmimi_ndryshuar; ?> &euro;</span>
        <?php } ?>
        
        <div class="single_post_more_info">
        <p class="list_taxonomies"><span class="label label-info">Modeli: <?php echo (isset($modeli) && $modeli != '') ? $modeli : $tipi_automjetit; ?></span><span class="label label-info">Seria: <?php echo $seria; ?></span><span class="label label-info">Lokacioni: <?php echo $lokacioni; ?></span></p>
	       <div class="social_addthis">
	      <ul class="leftZero">
	       <li><a class="addthis_button_facebook"><img src="<?php echo THEMEROOT; ?>/images/fb.png"></a></li>
	       <li><a class="addthis_button_twitter"><img src="<?php echo THEMEROOT; ?>/images/tw.png"></a></li>
	       <li><a class="addthis_button_google_plusone_share"><img src="<?php echo THEMEROOT; ?>/images/gp.png"></a></li>
	       <li><a class="addthis_button_pinterest_share"><img src="<?php echo THEMEROOT; ?>/images/pn.png"></a></li>
	       <li><a class="addthis_button_compact"><img src="<?php echo THEMEROOT; ?>/images/sh.png"></a></li>
	      </ul>
	      </div>
	 </div>
      </div><!-- /single-post-header -->
      <div id="main" role="main">
          <div class="slider">
            <div class="flexslider">
              <?php
              if(isset($cmimi_test) && !empty($cmimi_test)) {
              ?>
              <ul class="price_details leftZero">
              </ul>
        <?php
        }
            ?>
              <ul class="slides">
                <?php
                $nav = '';
                $counter = 0;
                foreach($attachments as $attachment):
                  $nav .= '<li>'.wp_get_attachment_image($attachment->ID, 'thumbnail').'</li>';
                  $image_attr = wp_get_attachment_image_src($attachment->ID);
                  $image_full = wp_get_attachment_image_src($attachment->ID, 'large');
                ?>
            <li class="image-container" data-thumb="<?php echo $image_attr[0]; ?>">
              <a href="<?php echo $image_full[0]; ?>" class="gallery">
                <?php echo wp_get_attachment_image($attachment->ID, 'large'); ?>
              </a>
            </li>
                <?php
                $counter++;
                endforeach;
              ?>
              </ul>
            </div><!-- /flexslider -->
          </div><!-- /slider -->
          
        </div><!-- /main -->
        </article>
      </div><!-- /span8 -->
      
      <div class="span5 leftZero post_user_info">
      <!-- /kodi per ndryshim -->

  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h5 id="myModalLabel">Ndrysho shpalljen</h5>
        </div>

      <form name="frm_change_post" id="frm_change_post" method="post">
      <div class="modal-body">
          <div class="control-group">
            <label for="user_post_email">Email: </label>
            <input type="text" name="user_post_email" id="user_post_email" />
          </div>

          <div class="control-group">
            <label for="user_post_code">Shënoni kodin: </label>
            <input type="text" name="user_post_code" id="user_post_code" />
          </div>
      </div>

      <div class="modal-footer">
          <span id="small_loader"><img src="<?php echo get_template_directory_uri(); ?>/images/small_loader.gif" /></span>
          <span id="ajax_message" class="alert-new"></span>
          <button class="btn close-modal" data-dismiss="modal" name="close-model" aria-hidden="true">Mbyll</button>
          <button class="btn btn-primary" id="frm_change_post_submit" name="frm_change_post_submit" type="submit">OK</button>
      </div>
      </form>
    </div>
  </div>
        <div class="leftZero posted_listing">
          <div id="kudos_container"><?php echo do_shortcode('[kudos]'); ?></div>
          <h4 class="clearfix">Shpallja: <button id="change_button" class="btn btn-primary btn-lg pull-right" data-toggle="modal" data-target=".bs-example-modal-sm">Ndrysho shpalljen</button><button id="save_button" name="save_button" class="btn btn-success main-green btn-lg pull-right">Ruaj</button><span id="success_loader"><img src="<?php echo get_template_directory_uri(); ?>/images/success_loader.gif" /></span></h4>
          <div class="main_content"><?php echo the_content(); ?></div>
          <?php if (function_exists('wpfp_link')) { wpfp_link(); } ?> | <a href="<?php echo get_option('home') ?>/shpalljet-ruajtura/" class="savePostsLink"><img src="<?php echo THEMEROOT; ?>/images/favorite-folder.png" /></i>Shpalljet e ruajtura</a>                        
        </div>
        
        <div class="post_content_overview leftZero">
          <h4>Të dhënat për automjetin:</h4>
            <ul class="leftZero">
              <li><i class="icon icon-double-angle-right"></i>Viti i prodhimit: <span><?php echo $viti_prodhimit; ?></span></li>
              <li><i class="icon icon-double-angle-right"></i>Modeli: <span><?php echo ucfirst($tipi_automjetit)."  ".$modeli_automjetit; ?></span></li>
              <li><i class="icon icon-double-angle-right"></i>Kubikazhi: <span><?php echo $kubikazhi; ?></span></li>
              <li><i class="icon icon-double-angle-right"></i>Tipi i transmisionit: <span><?php echo ucfirst($tipi_transmisioni); ?></span></li>
              <li><i class="icon icon-double-angle-right"></i>Lënda djegëse: <span><?php echo ucfirst($lenda_djegese); ?></span></li>
              <li><i class="icon icon-double-angle-right"></i>Km. të kaluara: <span><?php echo $kilometra; ?></span></li>
            </ul>
        </div>

        <div class="leftZero">
          <h4>Të dhënat për kontakt:</h4>
          <ul class="leftZero contact_info">
            <li><i class="icon icon-user"></i><span><?php echo $emri_postuesit; ?></span></li>
            <li><i class="icon icon-envelope-alt"></i><span><?php echo $email_postuesit; ?></span></li>
            <li><i class="icon icon-phone"></i><span><?php echo $numri_kontaktues; ?></span></li>
          </ul> 
        </div>
      </div>
    </div>
  </div>
</section>

<section class="next_prev_post">
  <div class="container">
    <div class="span12 leftZero">
    <div class="span7 leftZero">
      <h3>Postimet paraprake:</h3>
    <ul class="leftZero">
      <li class="next_post">
      <?php
        $next_post = get_next_post();
        $next_thumb = get_the_post_thumbnail($next_post->ID, array(250, 200));
        $next_title = $next_post->post_title;
        $next_title = ac_limit_characters($next_title);
        next_post_link("%link", ''.$next_thumb.'', TRUE);
      ?>
      <?php if($next_post) : ?>
      <div class="clearfix"></div>
        <div class="view view-first">
          <a href=""><?php echo $next_thumb; ?></a>
          <div class="mask">
            <?php if($next_title) : ?>
            <h2><?php next_post_link('%link',$next_title); ?></h2>
            <?php endif; ?>
          </div>
        </div>
      <?php endif; ?>
      </li>

      <li class="previous_post">
        <?php
        $prev_post = get_previous_post();
        $prev_thumb = get_the_post_thumbnail($prev_post->ID, array(250, 200));
        $prev_title = $prev_post->post_title;
        $prev_title = ac_limit_characters($prev_title);
        previous_post_link('%link',''.$prev_thumb.'',TRUE);
        ?>
        <?php if($prev_post) : ?>
        <div class="clearfix"></div>
        <div class="view view-first">
          <a href=""><?php echo $prev_thumb; ?></a>
          <div class="mask">
            <?php if($prev_title) : ?>
            <h2><?php previous_post_link('%link', $prev_title); ?></h2>
            <?php endif; ?>
          </div>
        </div>
        <?php endif; ?>
      </li>
      </ul><!-- /next-prev post -->
      
      </div>
    <?php endwhile; ?>
      <h3>Postime të ngjajshme</h3>
      <div class="span5 leftZero">
        <?php
          $terms = get_the_terms($post->ID, 'lloji', 'string');
          $terms_id = wp_list_pluck($terms, 'term_id');

          $get_related_posts = new WP_Query(array(
              'post_type' => 'automjete',
              'tax_query' => array(
                array(
                  'taxonomy' => 'lloji',
                  'field' => 'id',
                  'terms' => $terms_id,
                  'operator' => 'IN'
                )
              ),
              'posts_per_page' => 4,
              'ignore_sticky_posts' => 1,
              'orderby' => 'rand',
              'post__not_in' => array($post->ID)
          ));

          if($get_related_posts->have_posts()):
            ?>
            <ul class="leftZero related_posts">
            <?php while($get_related_posts->have_posts()): $get_related_posts->the_post();
              ?>
              <li>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(100, 50)); ?></a>
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                <small>(<?php the_time('m/j/y');?>)</small>
              </li>
              <?php
              endwhile;
            ?>
            </ul>
            <?php 
          endif;
        ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>