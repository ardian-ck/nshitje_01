<?php
/**
 * Template Name: Kontakt
 */
?>

<?php get_header(); ?>

<section class="contact_container">
  <div class="container">
    <div class="row">
      <div class="span12">
      <h4>Kontakt </h4>
        <div id="kontakt" class="span6 offset2">
              <?php
  $response = "";
  function ac_contact_form_reponse($type, $message) {
    global $response;
    if($type  == "success") $response = "<div class='alert alert-success'>{$message}</div>";
    else $response = "<div class='error'>{$message}</div>";
    echo $response;
  }

  $not_human = "Nuk keni shenuar përgjigjen e saktë.";
  $missing_content = "Ju lutem plotësoni të dhënat e kërkuara.";
  $email_invalid = "Email që keni shënuar nuk është valide";
  $message_sent_again = "Mesazhi nuk mund të dërgohet, përsëri.";
  $message_unsent = "Mesazhi nuk është dërguar, provoni përsëri.";
  $message_sent = "Faleminderit ! Mesazhi i juaj është dërguar.";
  //
  $name = $_POST['message_name'];
  $email = $_POST['message_email'];
  $message = $_POST['message_text'];
  $human = $_POST['message_human'];
  $random_number = rand(5,10);
  ($security_counter) ? $security_counter == 1 : $security_counter = $_POST['submitted'];

  $to = "konktakt@nshitje.com"; //info@nshitje.com
  $subject = "Kontakt: Mesazh nga faqja";
  $headers = 'From:'.$email. "\r\n".'Reply-To:'.$email."\r\n";
  //$headers = "From: ".$email."rn"."Reply-To:".$email."rn";
  $home_url = home_url();
  if(!$human == 0) {
    if($human != 6) ac_contact_form_reponse("error", $not_human);
    else {
      //validojm email dhe te dhenat tjera
      if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        ac_contact_form_reponse("error", $email_invalid);
      else {
        if(empty($name) || empty($message)) {
          ac_contact_form_reponse("error", $missing_content);
        }
        else {
          //te dhenat ok
          $message = esc_attr($message);
          $name = esc_attr($name);
          //
          $sent = wp_mail($to, $subject, strip_tags($message), $headers);
          if($sent) { ac_contact_form_reponse("success", $message_sent); $security_counter++; unset($_POST['email']);}
          else { ac_contact_form_reponse("error", $message_unsent);}
          if($security_counter > 1) {
          	ac_contact_form_response("error", $message_sent_again);
          }
        }
      }
    }
  }
  else if($_POST['submitted']) ac_contact_form_reponse("error", $missing_content);
 
?>
	<div id="contact_letter"><img src="<?php echo THEMEROOT; ?>/images/contact.png" /></div>
          <form action="<?php the_permalink(); ?>" id="contact_form" class="contact" method="post" name="contact_form">
          <p><label for="name" class="message_name">Emri: <span>*</span> <br><input type="text" name="message_name" value="<?php echo esc_attr($_POST['message_name']); ?>"></label>
          <label for="message_email"  class="message_email">Email: <span>*</span> <br><input type="text" name="message_email" value="<?php echo esc_attr($_POST['message_email']); ?>"></label>
          </p>

          <p><label for="message_text" class="message_text">Mesazhi: <span>*</span> <br><textarea type="text" name="message_text" rows="6"><?php echo esc_textarea($_POST['message_text']); ?></textarea></label></p>

          <p class="message_submit"><label for="message_human" class="message_human">Plotëso: <span>*</span> <br><input type="text" name="message_human"> - 4 = 2 </label>
          <input type="hidden" name="submitted" value="<?php echo $security_counter; ?>">
          <input type="submit" class="btn btn-success" value="Dërgo mesazhin">
          </p>
          </form>
          <ul class="leftZero">
            <li>Na gjeni edhe përmes: </li>
            <li><i class="icon icon-envelope"></i> info@nshitje.com</li>
            <li><i class="icon icon-facebook"></i> facebook.com/nshitje </li>
            <li><i class="icon icon-twitter"></i> @nshitje </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>