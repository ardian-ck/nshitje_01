<?php

/**
 * Sets up the content width value based on the theme's design.
 * @see twentythirteen_content_width() for template-specific adjustments.
 */
if (!isset($content_width))
    $content_width = 604;

/**
 * Adds support for a custom header image.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Skriptat me rendesi per theme
 */
require get_template_directory() . '/functions/auto.php';
require get_template_directory() . '/functions/custom_metaboxes.php';
require get_template_directory().'/functions/helper.php';
/**
 * Twenty Thirteen only works in WordPress 3.6 or later.
 */
if (version_compare($GLOBALS['wp_version'], '3.6-alpha', '<'))
    require get_template_directory() . '/inc/back-compat.php';

function twentythirteen_setup() {
    /*
     * Makes Twenty Thirteen available for translation.
     *
     * Translations can be added to the /languages/ directory.
     * If you're building a theme based on Twenty Thirteen, use a find and
     * replace to change 'twentythirteen' to the name of your theme in all
     * template files.
     */
    load_theme_textdomain('twentythirteen', get_template_directory() . '/languages');

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, icons, and column width.
     */
    add_editor_style(array('css/editor-style.css', 'fonts/genericons.css', twentythirteen_fonts_url()));

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support('automatic-feed-links');

    // Switches default core markup for search form, comment form, and comments
    // to output valid HTML5.
    add_theme_support('html5', array('search-form', 'comment-form', 'comment-list'));

    /*
     * This theme supports all available post formats by default.
     * See http://codex.wordpress.org/Post_Formats
     */
    add_theme_support('post-formats', array(
        'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
    ));

    // This theme uses wp_nav_menu() in one location.
    register_nav_menu('primary', __('Navigation Menu', 'twentythirteen'));

    /*
     * This theme uses a custom image size for featured images, displayed on
     * "standard" posts and pages.
     */
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(604, 270, true);

    // This theme uses its own gallery styles.
    add_filter('use_default_gallery_style', '__return_false');
}

add_action('after_setup_theme', 'twentythirteen_setup');

/**
 * Returns the Google font stylesheet URL, if available.
 *
 * The use of Source Sans Pro and Bitter by default is localized. For languages
 * that use characters not supported by the font, the font can be disabled.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string Font stylesheet or empty string if disabled.
 */
function twentythirteen_fonts_url() {
    $fonts_url = '';

    /* Translators: If there are characters in your language that are not
     * supported by Source Sans Pro, translate this to 'off'. Do not translate
     * into your own language.
     */
    $source_sans_pro = _x('on', 'Source Sans Pro font: on or off', 'twentythirteen');

    /* Translators: If there are characters in your language that are not
     * supported by Bitter, translate this to 'off'. Do not translate into your
     * own language.
     */
    $bitter = _x('on', 'Bitter font: on or off', 'twentythirteen');

    if ('off' !== $source_sans_pro || 'off' !== $bitter) {
        $font_families = array();

        if ('off' !== $source_sans_pro)
            $font_families[] = 'Source Sans Pro:300,400,700,300italic,400italic,700italic';

        if ('off' !== $bitter)
            $font_families[] = 'Bitter:400,700';

        $query_args = array(
            'family' => urlencode(implode('|', $font_families)),
            'subset' => urlencode('latin,latin-ext'),
        );
        $fonts_url = add_query_arg($query_args, "//fonts.googleapis.com/css");
    }

    return $fonts_url;
}
//pagination ne shqip
if ( ! function_exists( 'ac_page_to_faqe' ) )
{
    register_activation_hook(   __FILE__ , 'ac_flush_rewrite_on_init' );
    register_deactivation_hook( __FILE__ , 'ac_flush_rewrite_on_init' );
    add_action( 'init', 'ac_page_to_faqe' );

    function ac_page_to_faqe()
    {
        $GLOBALS['wp_rewrite']->pagination_base = 'faqe';
    }

    function ac_flush_rewrite_on_init()
    {
        add_action( 'init', 'flush_rewrite_rules', 11 );
    }
}

define('THEMEROOT', get_template_directory_uri()); //localhost/projekte/auto_wp/wp-content/themes/auto
define('IMAGESROOT', THEMEROOT . '/images/'); //localhost/projekte/auto_wp/wp-content/themes/auto/images/

/* ------------------------
 *      SKRIPTAT 
 * --------------------- */

function ac_scripts() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, 'latest', false);
        wp_enqueue_script('jquery');
    }
    
    wp_register_script('jquery', THEMEROOT.'/js/jquery.js', false, '', false);
    wp_enqueue_script('jquery');
    
    //wp_enqueue_script('jqueryui', '//code.jquery.com/ui/1.10.3/jquery-ui.js', array('jquery'), '', true);
    //wp_enqueue_script('jqueryui');

    wp_register_script('scroller', THEMEROOT . '/js/jquery.li-scroller.1.0.js', array('jquery'), '', true);
    wp_enqueue_script('scroller');

    wp_register_script('minimalect', THEMEROOT . '/js/jquery.minimalect.min.js', array('jquery'), '', true);
    wp_enqueue_script('minimalect');

    if(is_home()) {
        wp_register_script('main', THEMEROOT . '/js/main.js', array('jquery'), '', true);
        wp_localize_script('main', 'main_ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
        wp_enqueue_script('main');
    }

    if(!is_home()) {
        //if is_single -> 
        wp_register_script('flexslider', THEMEROOT.'/js/jquery.flexslider-min.js', array('jquery'),'', true);
        wp_enqueue_script('flexslider');
        
    }
    
    if(is_page('shpallje') || !is_home()) {
        wp_register_script('bootstrap','//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js', array('jquery'), '', true);
        wp_enqueue_script('bootstrap');
        
        wp_register_script('icheck', THEMEROOT . '/js/jquery.icheck.min.js', array('jquery'), '', true);
        wp_enqueue_script('icheck');

        wp_register_script('validation', 'http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js', array('jquery'),'', true);
        wp_enqueue_script('validation');

        wp_register_script('masked_inputs', THEMEROOT.'/js/jquery.maskedinput.js', array('jquery'),'', true);
        wp_enqueue_script('masked_inputs');

        wp_register_script('bootstrap_upload', THEMEROOT.'/js/bootstrap-fileupload.min.js', array('jquery'),'', true);
        wp_enqueue_script('bootstrap_upload');
        
        wp_register_script('bootstrap-tooltip', THEMEROOT.'/js/tooltip.js', array('jquery'), '', true);
        wp_enqueue_script('bootstrap-tooltip');
        //endif
        wp_register_script('colorbox', THEMEROOT.'/js/jquery.colorbox-min.js', array('jquery'),'', true);
        wp_enqueue_script('colorbox');
        
        //vetem per formen e shpalljeve
        wp_register_script('pnotify', THEMEROOT.'/js/jquery.pnotify.min.js', array('jquery'),'', true);
        wp_enqueue_script('pnotify');
        
        wp_register_script('uipunch', THEMEROOT.'/js/jquery.ui.touch-punch.min.js', array('jquery'),'', true);
        wp_enqueue_script('uipunch');

        wp_register_script('uisortable', THEMEROOT.'/js/jquery-ui.sortable.min.js', array('jquery'),'', true);
        wp_enqueue_script('uisortable');

        wp_register_script('mixitup', THEMEROOT.'/js/jquery.mixitup.min.js', array('jquery'),'', true);
        wp_enqueue_script('mixitup');

        wp_register_script('app', THEMEROOT . '/js/app.js', array('jquery'), '', true);
        wp_localize_script('app', 'main_ajax', array('ajaxurl' => admin_url('admin-ajax.php'),'nonce' => wp_create_nonce("anunaki")));
        wp_enqueue_script('app');
    }

    if(is_single()) {
        //inline edit
        wp_register_script('bootstrap_editable', THEMEROOT.'/js/bootstrap-editable.min.js', array('jquery'),'', true);
        wp_enqueue_script('bootstrap_editable');
    }

    wp_register_script('bootstrap_collapse', THEMEROOT.'/js/bootstrap-collapse.js', array('jquery'),'', true);
    wp_enqueue_script('bootstrap_collapse');

   /* wp_register_script('bootstrap_modal', THEMEROOT.'/js/bootstrap-modal.js', array('jquery'),'', true);
    wp_enqueue_script('bootstrap_modal');*/
  
}

add_action('wp_enqueue_scripts', 'ac_scripts');

/* ------------------------
 *      CSS 
 * --------------------- */

function ac_styles() {
    if(is_admin()) {
        wp_register_style('jquery-ui-css', THEMEROOT.'/css/jquery-ui-custom.css', array(), '', 'all');
        wp_enqueue_style('jquery-ui-css');
    }

    wp_register_style('font-awesome','//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css', array(), '', 'all');
    wp_enqueue_style('font-awesome');

    wp_register_style('bootstrap', THEMEROOT . '/css/bootstrap.min.css', array(), '', 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('bootstrap-responsive', THEMEROOT . '/css/bootstrap-responsive.min.css', array(), '', 'all');
    wp_enqueue_style('bootstrap-responsive');

    wp_register_style('app', THEMEROOT . '/css/app.css', array(), '', 'all');
    wp_enqueue_style('app');

    wp_register_style('bootstrap_editable_style', THEMEROOT . '/css/bootstrap-editable.css', array(), '', 'all');
    wp_enqueue_style('bootstrap_editable_style');
}

add_action('wp_enqueue_scripts', 'ac_styles');

function _remove_script_version( $src ){
    $parts = explode( '?', $src );
    return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );



// add_action('wp_ajax_get_model', 'get_model_init');
// add_action('wp_ajax_nopriv_get_model', 'get_model_init');
// function get_model_init(){
//     $id_auto  = $_POST['id_auto'];
//     $tipi_auto = $_POST['tipi_auto'];
//     $taxonomy = 'tipi';

//     $sub_cat = get_term_children($id_auto, $taxonomy);
//     echo '<select name="modeli_auto" id="modeli_auto">';
//     foreach($sub_cat as $subcategory) {
//         $term = get_term_by('id', $subcategory, $taxonomy);
//         echo '<option name="'.$term->name.'" id="'.$term->term_id.' data-parent="'.$term->parent.'" data-taxonomy="'.$term->taxonomy.'">'.$term->name.'</option>';
//     }
    
//     echo '</select>';
//     die();
// }

/**
 * AJAX
 */
//add_action( 'wp_ajax_my_action', 'my_action_callback' );
add_action('wp_ajax_nopriv_change_user_post', 'ac_change_user_post');
add_action('wp_ajax_change_user_post', 'ac_change_user_post');
function ac_change_user_post() {
    $error = array();

    check_ajax_referer('anunaki', 'nonce');
    //check if post_id + code match
    if(isset($_POST['post_id'])) {
        $post_id = intval($_POST['post_id']);
    } else {
        $error[] = "Ka ndodhur një gabim.";
    }

    if(isset($_POST['user_code'])) {
        $post_code = filter_var($_POST['user_code'], FILTER_SANITIZE_STRING);
    } else {
        $error[] = "Ju lutem shënoni kodin.";
    }

    if(isset($_POST['user_email'])) {
        if(!filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
        $error[] = "Nuk është email valide.";
        } else {
            $user_email = filter_var($_POST['user_email'], FILTER_SANITIZE_EMAIL);
        }
    } else {
        $error[] = "Ju lutem shënoni email-in.";
    }
    
    if(empty($error)) {
        $checked = check_post_code($post_id, $user_email, $post_code); // returns $post or false

        if($checked !== false ) {
            //post exists
            $response = [
                "flag" => true,
                "data" => $checked
            ];
        } else {
            //posts not exists
            $response = [
                "flag" => false,
                "data" => "Ka ndodhur një gabim. Të dhënat nuk janë në rregull."
            ];
        }
    } else {
        $response = [
            "flag" => false,
            "data" => $error
        ];
    }

    echo json_encode($response);
    die();
}
/**
 * Checks if posts exists.
 * @param  [type] $post_id
 * @param  [type] $user_email
 * @param  [type] $post_code
 * @return true -> int, false -> boolean
 */
function check_post_code($post_id, $user_email, $post_code) {
    global $wpdb;

    $args = array(
        'p' => $post_id,
        'post_type' => 'automjete',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'rand',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'ac_kodi',
                'value' => $post_code,
                'compare' => '='
            ),
            array(
                'key' => 'ac_email_postuesit',
                'value' => $user_email,
                'compare' => '='
            )
        )
    );

    $post = new WP_Query($args);

    if($post->found_posts == 1) {
        $result = $post_id;
    } else {
        $result = false;
    }

    return $result;
}

add_action('wp_ajax_update_user_data', 'update_user_data_callback');
add_action('wp_ajax_nopriv_update_user_data', 'update_user_data_callback');
function update_user_data_callback() {
    $error = array();

    if(isset($_POST) && isset($_POST['post_id'])) {
        $post_id = intval($_POST['post_id']);
        
        if(isset($_POST['content']) && $_POST['content'] != '') {
            $post_content = filter_var($_POST['content'], FILTER_SANITIZE_STRING);
            $post_info = array(
                'ID' => $post_id,
                'post_content' => $post_content,
                'post_type' => 'automjete',
                'post_status' => 'pending'
            );
            wp_update_post($post_info);
            $response = [
                "flag" => true,
                "data" => "Të dhënat janë ndryshuar me sukses. (shpallja)"
            ];
        }

        if(isset($_POST['price']) && $_POST['price'] != '') {
            $car_price = filter_var($_POST['price'], FILTER_SANITIZE_NUMBER_INT);
            update_post_meta($post_id, 'ac_cmimi_auto', $car_price);
            $response = [
                "flag" => true,
                "data" => "Të dhënat janë ndryshuar me sukses. (cmimi)"
            ];
        }

        if(isset($_POST['title']) && $_POST['title'] != '') {
            $car_title = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
            $post_title = array(
                'ID' => $post_id,
                'post_title' => $car_title,
                'post_type' => 'automjete',
                'post_status' => 'pending'
            );
            wp_update_post($post_title);
            $response = [
                "flag" => true,
                "data" => "Të dhënat janë ndryshuar me sukses (titulli)."
            ];
        }

    } else {
        $response = [
            "flag" => false,
            "data" => "Kanë ndodhur gabime."
        ];
    }
    echo json_encode($response);
    die();
}

add_action('wp_ajax_get_serie_init', 'get_serie_init');
add_action('wp_ajax_nopriv_get_serie_init', 'get_serie_init');

function get_serie_init() {
    global $wpdb;
    //$model_id = $_POST['model_id'];
    $model_id = $_REQUEST['model_id'];

    $result = $wpdb->get_results("SELECT `wp_auto_list`.id, `wp_auto_list`.model, `wp_auto_serie_list`.model_id, `wp_auto_serie_list`.serie FROM wp_auto_list INNER JOIN wp_auto_serie_list ON `wp_auto_list`.id = `wp_auto_serie_list`.model_id WHERE `wp_auto_list`.model = '$model_id' ");
    //echo '<select>';
    echo '<option value="">-- të gjitha --</option>';
    foreach($result as $res) {

        echo '<option value="'.$res->serie.'">'.$res->serie.'</option>';
        //$data = $res;
    }
    
    //echo '</select>';
    die();
}

/**
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function twentythirteen_wp_title($title, $sep) {
    global $paged, $page;

    if (is_feed())
        return $title;

    // Add the site name.
    $title .= get_bloginfo('name');

    // Add the site description for the home/front page.
    $site_description = get_bloginfo('description', 'display');
    if ($site_description && ( is_home() || is_front_page() ))
        $title = "$title $sep $site_description";

    // Add a page number if necessary.
    if ($paged >= 2 || $page >= 2)
        $title = "$title $sep " . sprintf(__('Page %s', 'twentythirteen'), max($paged, $page));

    return $title;
}

add_filter('wp_title', 'twentythirteen_wp_title', 10, 2);

/**
 * Registers two widget areas.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function twentythirteen_widgets_init() {
    register_sidebar(array(
        'name' => __('Main Widget Area', 'twentythirteen'),
        'id' => 'sidebar-1',
        'description' => __('Appears in the footer section of the site.', 'twentythirteen'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => __('Secondary Widget Area', 'twentythirteen'),
        'id' => 'sidebar-2',
        'description' => __('Appears on posts and pages in the sidebar.', 'twentythirteen'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}

add_action('widgets_init', 'twentythirteen_widgets_init');

if (!function_exists('twentythirteen_paging_nav')) :

    /**
     * Displays navigation to next/previous set of posts when applicable.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_paging_nav() {
        global $wp_query;

        // Don't print empty markup if there's only one page.
        if ($wp_query->max_num_pages < 2)
            return;
        ?>
        <nav class="navigation paging-navigation" role="navigation">
            <h1 class="screen-reader-text"><?php _e('Posts navigation', 'twentythirteen'); ?></h1>
            <div class="nav-links">

                <?php if (get_next_posts_link()) : ?>
                    <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Older posts', 'twentythirteen')); ?></div>
                <?php endif; ?>

        <?php if (get_previous_posts_link()) : ?>
                    <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', 'twentythirteen')); ?></div>
        <?php endif; ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
        <?php
    }

endif;

if (!function_exists('twentythirteen_post_nav')) :

    /**
     * Displays navigation to next/previous post when applicable.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_post_nav() {
        global $post;

        // Don't print empty markup if there's nowhere to navigate.
        $previous = ( is_attachment() ) ? get_post($post->post_parent) : get_adjacent_post(false, '', true);
        $next = get_adjacent_post(false, '', false);

        if (!$next && !$previous)
            return;
        ?>
        <nav class="navigation post-navigation" role="navigation">
            <h1 class="screen-reader-text"><?php _e('Post navigation', 'twentythirteen'); ?></h1>
            <div class="nav-links">

        <?php previous_post_link('%link', _x('<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'twentythirteen')); ?>
        <?php next_post_link('%link', _x('%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'twentythirteen')); ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
        <?php
    }

endif;

if (!function_exists('twentythirteen_entry_meta')) :

    /**
     * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
     *
     * Create your own twentythirteen_entry_meta() to override in a child theme.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_entry_meta() {
        if (is_sticky() && is_home() && !is_paged())
            echo '<span class="featured-post">' . __('Sticky', 'twentythirteen') . '</span>';

        if (!has_post_format('link') && 'post' == get_post_type())
            twentythirteen_entry_date();

        // Translators: used between list items, there is a space after the comma.
        $categories_list = get_the_category_list(__(', ', 'twentythirteen'));
        if ($categories_list) {
            echo '<span class="categories-links">' . $categories_list . '</span>';
        }

        // Translators: used between list items, there is a space after the comma.
        $tag_list = get_the_tag_list('', __(', ', 'twentythirteen'));
        if ($tag_list) {
            echo '<span class="tags-links">' . $tag_list . '</span>';
        }

        // Post author
        if ('post' == get_post_type()) {
            printf('<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>', esc_url(get_author_posts_url(get_the_author_meta('ID'))), esc_attr(sprintf(__('View all posts by %s', 'twentythirteen'), get_the_author())), get_the_author()
            );
        }
    }

endif;

if (!function_exists('twentythirteen_entry_date')) :

    /**
     * Prints HTML with date information for current post.
     *
     * Create your own twentythirteen_entry_date() to override in a child theme.
     *
     * @since Twenty Thirteen 1.0
     *
     * @param boolean $echo Whether to echo the date. Default true.
     * @return string The HTML-formatted post date.
     */
    function twentythirteen_entry_date($echo = true) {
        if (has_post_format(array('chat', 'status')))
            $format_prefix = _x('%1$s on %2$s', '1: post format name. 2: date', 'twentythirteen');
        else
            $format_prefix = '%2$s';

        $date = sprintf('<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>', esc_url(get_permalink()), esc_attr(sprintf(__('Permalink to %s', 'twentythirteen'), the_title_attribute('echo=0'))), esc_attr(get_the_date('c')), esc_html(sprintf($format_prefix, get_post_format_string(get_post_format()), get_the_date()))
        );

        if ($echo)
            echo $date;

        return $date;
    }

endif;

if (!function_exists('twentythirteen_the_attached_image')) :

    /**
     * Prints the attached image with a link to the next attached image.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_the_attached_image() {
        $post = get_post();
        $attachment_size = apply_filters('twentythirteen_attachment_size', array(724, 724));
        $next_attachment_url = wp_get_attachment_url();

        /**
         * Grab the IDs of all the image attachments in a gallery so we can get the URL
         * of the next adjacent image in a gallery, or the first image (if we're
         * looking at the last image in a gallery), or, in a gallery of one, just the
         * link to that image file.
         */
        $attachment_ids = get_posts(array(
            'post_parent' => $post->post_parent,
            'fields' => 'ids',
            'numberposts' => -1,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'order' => 'ASC',
            'orderby' => 'menu_order ID'
                ));

        // If there is more than 1 attachment in a gallery...
        if (count($attachment_ids) > 1) {
            foreach ($attachment_ids as $attachment_id) {
                if ($attachment_id == $post->ID) {
                    $next_id = current($attachment_ids);
                    break;
                }
            }

            // get the URL of the next image attachment...
            if ($next_id)
                $next_attachment_url = get_attachment_link($next_id);

            // or get the URL of the first image attachment.
            else
                $next_attachment_url = get_attachment_link(array_shift($attachment_ids));
        }

        printf('<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>', esc_url($next_attachment_url), the_title_attribute(array('echo' => false)), wp_get_attachment_image($post->ID, $attachment_size)
        );
    }

endif;

/**
 * Returns the URL from the post.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string The Link format URL.
 */
function twentythirteen_get_link_url() {
    $content = get_the_content();
    $has_url = get_url_in_content($content);

    return ( $has_url ) ? $has_url : apply_filters('the_permalink', get_permalink());
}

/**
 * Extends the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function twentythirteen_body_class($classes) {
    if (!is_multi_author())
        $classes[] = 'single-author';

    if (is_active_sidebar('sidebar-2') && !is_attachment() && !is_404())
        $classes[] = 'sidebar';

    if (!get_option('show_avatars'))
        $classes[] = 'no-avatars';

    return $classes;
}

add_filter('body_class', 'twentythirteen_body_class');

/**
 * Adjusts content_width value for video post formats and attachment templates.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function twentythirteen_content_width() {
    global $content_width;

    if (is_attachment())
        $content_width = 724;
    elseif (has_post_format('audio'))
        $content_width = 484;
}

add_action('template_redirect', 'twentythirteen_content_width');

/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 * @return void
 */
function twentythirteen_customize_register($wp_customize) {
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
    $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';
}

add_action('customize_register', 'twentythirteen_customize_register');

/**
 * Binds JavaScript handlers to make Customizer preview reload changes
 * asynchronously.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_customize_preview_js() {
    wp_enqueue_script('twentythirteen-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array('customize-preview'), '20130226', true);
}

//rewrite rules per custom post type.
if(is_admin() && isset($_GET['activated']) && $pagenow == 'themes.php') {
    $wp_rewrite->flush_rules();
}

add_action('customize_preview_init', 'twentythirteen_customize_preview_js');

add_theme_support('post-thumbnails');
add_image_size('featured-preview', 75, 75, true);
add_image_size('index-preview', 280, 165, true);