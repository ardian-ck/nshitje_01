 <!-- Modal / Help -->
<div id="modalHelp" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Si të publikoni shpallje ?</h3>
  </div>
  <div class="modal-body">
    <ol>
        <li>
            <p>Kliko në linkun <i class="icon icon-hand-right"></i>&nbsp;<a href="<?php echo get_option('home') ?>/shpallje/" class="btn btn-success btn-large main-green" target="blank" style="color:#fff;">Shtoni shpallje</a></p>
        </li>
        <li>
            <p>Plotësoni me kujdes fushat e kërkuara. <i class="icon icon-list-ol"></i></p>
            <img src="<?php echo THEMEROOT; ?>/images/help.png" alt="" />
        </li>
        <li>
            <p>Mos harroni të shtoni fotot! <em>(1 deri në 4)</em> <i class="icon icon-warning-sign"></i></p>
        </li>
        <li>
            <p>
                <p>Pasi të keni plotësuar klikoni në butonin për të shtuar shpallje.</p>
            </p>
        </li>

        <li>
            <p>Prisni pak. . .</p>
        </li>

        <li>
            <p>
                Nëse nuk ka pasur ndonjë gabim, një mesazh do t'ju tregoj se shpallja është shtuar. Pas miratimit shpallja do të jetë në faqen tonë.<i class="icon icon-ok"></i>
            </p>
        </li>

        <strong>Në email-in të cilin keni dërguar shpalljen do të gjeni një mesazh konfirmimi, me disa të dhëna tjera me rëndësi.<br /></strong>

        <em>Mund të kontaktoni përmes: <a href="mailto:info@nshitje.com"> info@nshitje.com</a></em>
    </ol>
  </div>
  <div class="modal-footer">
    <button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true">Mbyll</button>
  </div>
</div><!-- /modal -->