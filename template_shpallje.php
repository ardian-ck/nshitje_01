<?php
/**
 * Template Name: Shto Shpallje
 */

ob_start();
?>
<?php
global $wpdb;
$error = array();
$prefix = "ac_";

if ('POST' == $_SERVER['REQUEST_METHOD'] && !empty($_POST['ac_action']) && $_POST['ac_action'] == "automjete")
     {
    if (isset($_POST['ac_shto_shpallje']) && wp_verify_nonce($_POST['ac_ruaj_shpallje_nonce'], basename(__FILE__))) {
        //var_dump($_POST);
        //marrim te dhenat.
        if ($_POST['ac_shpallja_excerpt'] != '') {
            $ac_shpallja_excerpt = sanitize_text_field($_POST['ac_shpallja_excerpt']);
        } else {
            $error[] .= "Shtoni titullin e shpalljes. Jo më shumë se 100 karaktetere.";
        }

        $token_id = stripslashes($_POST['ac_token']);
        //taxonomite.
        $lloji = sanitize_text_field($_POST['lloji']);
        //viti i prodhimit
        if(isset($_POST['select_year']) && $_POST['select_year'] != '') {
           $ac_viti_prodhimit = $_POST['select_year']; //viti i prodhimit te vetures
        }
        elseif(isset($_POST['ac_agro_year']) && $_POST['ac_agro_year'] != '') {
            $ac_viti_prodhimit = $_POST['ac_agro_year']; //viti i prodhimit te makines bujqesore
        }
        elseif(isset($_POST['ac_truck_year']) && $_POST['ac_truck_year'] != '') {
            $ac_viti_prodhimit = $_POST['ac_truck_year']; //viti i prodhimit te kamionit.
        }
        elseif(isset($_POST['ac_build_year']) && $_POST['ac_build_year'] != '') {
            $ac_viti_prodhimit = $_POST['ac_build_year']; //viti i prodhimit te makines ndert.
        }
        elseif(isset($_POST['ac_motor_year']) && $_POST['ac_motor_year'] != '') {
            $ac_viti_prodhimit = $_POST['ac_motor_year']; //viti i prodhimit te motorit
        }
        
        if(isset($_POST['select_modeli']) && $_POST['select_modeli'] != '') {
            $ac_tipi_automjetit = $_POST['select_modeli']; //veture
        }
        elseif(isset($_POST['ac_select_agro']) && $_POST['ac_select_agro'] != '') {
            $ac_tipi_automjetit = $_POST['ac_select_agro']; //makina bujqesore
        }
        elseif(isset($_POST['ac_build_truck']) && $_POST['ac_build_truck'] != '') {
            $ac_tipi_automjetit = $_POST['ac_build_truck']; //makina ndertimtarie
        }
        elseif(isset($_POST['ac_select_truck']) && $_POST['ac_select_truck'] != '') {
            $ac_tipi_automjetit = $_POST['ac_select_truck']; //kamion
        }
        elseif(isset($_POST['ac_motor_model']) && $_POST['ac_motor_model'] != '') {
            $ac_tipi_automjetit = $_POST['ac_motor_model']; //motor
        }

        if(isset($_POST['select_serie']) && $_POST['select_serie'] != '') {
            $modeli = $_POST['select_serie']; //seria e veturave
        }
        elseif(isset($_POST['ac_agro_serie']) && $_POST['ac_agro_serie'] != '') {
            $modeli = $_POST['ac_agro_serie']; //seria e makines bujqesore
        }
        elseif(isset($_POST['ac_build_serie']) && $_POST['ac_build_serie'] != '') {
            $modeli = $_POST['ac_build_serie']; //seria e makinave ndertim.
        }
        elseif(isset($_POST['ac_truck_serie']) && $_POST['ac_truck_serie'] != '') {
            $modeli = $_POST['ac_truck_serie']; //seria e kamioneve
        }
        elseif(isset($_POST['ac_motor_serie']) && $_POST['ac_motor_serie'] != '') {
            $modeli = $_POST['ac_motor_serie']; //seria e motoreve
        }

        $ac_motor_size = $_POST['ac_motor_size'];
        $lenda_djegese = sanitize_text_field($_POST['lenda_djegese']);
        $ac_kubikazhi = sanitize_text_field($_POST['ac_kubikazhi']);
        $transmisioni = sanitize_text_field($_POST['transmisioni']);
        $lokacioni = sanitize_text_field($_POST['lokacioni']);

        $ac_km_kaluara = $_POST['ac_km_kaluara'];
        if ($_POST['ac_cmimi_auto'] != '' && is_numeric($_POST['ac_cmimi_auto'])) {
            $ac_cmimi_auto = sanitize_text_field($_POST['ac_cmimi_auto']);
        } else {
            $error[] .= "Shënoni çmimin e automjetit, duhet të jetë numër pa presje dhjetore apo pikë.";
        }

        $ac_checkbox_cmimi = $_POST['ac_checkbox_cmimi']; //array
        if ($_POST['ac_pershkrimi'] != '') {
            $ac_pershkrimi = esc_textarea($_POST['ac_pershkrimi']);
        } else {
            $error[] .= "Jepni një përshkrim për automjetin.";
        }

        //te dhenat personale.
        if ($_POST['ac_emri_postuesit'] != '') {
            $ac_emri_postuesit = sanitize_text_field($_POST['ac_emri_postuesit']);
        } else {
            $error[] .= "Ju lutem shënoni emrin.";
        }

        if ($_POST['ac_email_postuesit'] != '') {
            $ac_email_postuesit = sanitize_email($_POST['ac_email_postuesit']);
        } else {
            $error[] .= "Ju lutem shënoni një email valide.";
        }

        if ($_POST['ac_nr_postuesit'] != '') {
            $ac_nr_postuesit = sanitize_text_field($_POST['ac_nr_postuesit']);
        } else {
            $error[] .= "Ju lutem shënoni numrin e telefonit.";
        }

        if($_POST['ac_parulla'] != '') {
            $ac_parulla = sanitize_text_field($_POST['ac_parulla']);
        } else {
            $error[] .= "Shënoni një parullë për shpalljen.";
        }

        //kjo do te perdoret per te treguar nese posti eshte valid.
        $ac_valide = 1;
        //numri i mundesive per ndryshime, perdoret nga postuesi i shpalljes
        $ac_nr_ndryshimeve = 0;

        $ac_post_submitted_date = the_date('F j, Y g:i a');
        $ac_post_date = current_time('mysql', 0);
        $post_identification = md5(uniqid(rand(), true)); //32 number identification per postin

        //ruajm te dhenat
        $post_data = array(
            'post_title' => wp_strip_all_tags($ac_shpallja_excerpt),
            'post_content' => wp_strip_all_tags($ac_pershkrimi),
            'post_author' => $ac_emri_postuesit . ' (' . $ac_email_postuesit . ')',
            'post_date' => $ac_post_submitted_date,
            'post_status' => 'pending',
            'post_author' => wp_strip_all_tags($ac_email_postuesit),
            'post_type' => 'automjete'
        );

        //shtojme imazhet.
        if ($_FILES) {
               foreach ($_FILES as $file => $array) {
                //merr vektorin e fajllave te shtuar.
                //krahaso nese ate vektor gjenden dy fajlla me emer dhe madhesi te njejt.
                if (isset($_FILES[$file])) {
                    ($_FILES[$file]['size'] > 0) ? $number_of_photos++ : $number_of_photos;
                    if($number_of_photos > 0 && $number_photos <= 4) {
                        if($_FILES[$file]['size'] > 0) {
                            $tmpName = $_FILES[$file]['tmp_name'];

                            list($width, $height, $type, $attr) = getimagesize($tmpName);

                            $arr_file_type = wp_check_filetype(basename($_FILES[$file]['name']));
                            $uploaded_file_type = $arr_file_type['type'];
                            $allowed_file_types = array('image/jpeg', 'image/jpg', 'image/png', 'image/gif');

                            if (!in_array($uploaded_file_type, $allowed_file_types)) {
                                $error[] .= "Ju lutem shtoni fajlla të tipit JPG, JPEG, GIF apo PNG";
                            }
                            if(($_FILES[$file]['size'] > 4145728) || $width > 4000 || $height > 4000 ) {
                                $error[] .= "Imazhet janë të mëdha, ju lutem shtoni imazhet deri në 4MB.";
                                unlink($tmpName);
                            }
                        }
                        
                    }//if($number_of_photos)
                    else {
                        $error[] .= "Shtoni një deri në katër foto.";
                    }
                }//if($_FILES($file) && $_FILES[$file]['size'] > 0 )
                else {
                    $error[] .= "Ju lutem shtoni fotot.";
                }
            }//foreach
        }//if($_FILES)

        if(empty($error) ) {
            $post_id = wp_insert_post($post_data);
            if ($post_id) {          
                //shtojm metadata ne post.
                add_post_meta($post_id, 'ac_shpallja_excerpt', $ac_shpallja_excerpt, true);
                add_post_meta($post_id, 'lloji', $lloji, true);
                add_post_meta($post_id, 'tipi', $ac_tipi_automjetit, true);
                add_post_meta($post_id, 'modeli', $modeli, true);
                add_post_meta($post_id, 'madhesia_motorit', $ac_motor_size, true);
                add_post_meta($post_id, 'ac_viti_prodhimit', $ac_viti_prodhimit, true);
                add_post_meta($post_id, 'lenda_djegese', $lenda_djegese, true);
                add_post_meta($post_id, 'ac_kubikazhi', $ac_kubikazhi, true);
                add_post_meta($post_id, 'transmisioni', $transmisioni, true);
                add_post_meta($post_id, 'ac_km_kaluara', $ac_km_kaluara, true);
                add_post_meta($post_id, 'ac_cmimi_auto', $ac_cmimi_auto, true);
                add_post_meta($post_id, 'ac_cmimi_test', $ac_checkbox_cmimi, true);
                add_post_meta($post_id, 'ac_data_publikimit', $ac_post_date, true);
                add_post_meta($post_id, 'ac_timestamp', $post_identification, true);

                add_post_meta($post_id, 'ac_pershkrimi', $ac_pershkrimi, true);
                add_post_meta($post_id, 'lokacioni', $lokacioni, true);
                add_post_meta($post_id, 'ac_emri_postuesit', $ac_emri_postuesit, true);
                add_post_meta($post_id, 'ac_email_postuesit', $ac_email_postuesit, true);
                add_post_meta($post_id, 'ac_nr_postuesit', $ac_nr_postuesit, true);
                add_post_meta($post_id, 'ac_valide', $ac_valide, true);
                add_post_meta($post_id, 'ac_nr_ndryshimeve', $ac_nr_ndryshimeve, true);
                
                /** updated - code for post added from user **/
                add_post_meta($post_id, 'ac_kodi', $ac_parulla, true);

                //check code - meta field
                /*$random_string = ac_generate_random_string();
                add_post_meta($post_id, 'ac_kodi', $random_string, true);*/

                
                //update modelin e automjetit
                if($ac_tipi_automjetit !== '') {
                $selected_model = strtolower(str_replace(' ', '-', $ac_tipi_automjetit));

                $terms = get_terms('modeli', 'orderby=count&hide_empty=0');
                //marrim krejt elementet prej custom taxonomy
                $term_name = array();
                foreach($terms as $term) {
                    $term_name[] = $term->slug;
                    if($selected_model == $term->slug) {
                        $term_req = $term->slug;
                    }
                }
                if(in_array($selected_model, $term_name)){
                    //nese $selected_model eshte ne term_name wp_set_post_term
                    //Problemi: Kthe value ne slug dhe jo id sic eshte per momentin.
                    //nese $selected_model = $term_name dhe marrim id nga custom + update.
                    $custom_selected_id = $wpdb->get_var("SELECT `wp_terms`.term_id FROM `wp_terms` WHERE `wp_terms`.slug = '$term_req'");
                    }
                    wp_set_post_terms($post_id, $custom_selected_id, 'modeli');
                }

                //shtojm edhe te dhenat per taxonomite.
                wp_set_post_terms($post_id, $lloji, 'lloji');
                wp_set_post_terms($post_id, $transmisioni, 'transmisioni');
                wp_set_post_terms($post_id, $lokacioni, 'lokacioni');
                wp_set_post_terms($post_id, $lenda_djegese, 'lenda_djegese');

                if ($_FILES) {
                    foreach ($_FILES as $file => $array) {
                        if($_FILES[$file]['size'] > 0) {
                            $new_upload = ac_insert_attachment($file, $post_id);
                        }
                    }
                }
				
                
				
                wp_set_post_tags($post_id, $_POST['post_tags']);
                
                $public_id = $post_id * 1498158 + 825920496;
                $success = 'Pasi të miratohet, shpallja do të jetë në faqen tonë. Klikoni në linkun e dërguar në email-in e shënuar, për të larguar apo ndryshuar  shpalljen.';

                $weburl = get_option('home');

                $message = '<h3>Faleminderit !</h3>';
                $message .= '<p>Shpallja është dërguar me sukses, pasi të miratohet ajo do të jetë në faqen tonë.</p>';
                $message .= '<p>Klikoni në linkun më poshtë nëse dëshironi të <strong>ndryshoni / largoni</strong> shpalljen.</p>';
                $message .= '<p>Për të ndryshuar çmimin e shpalljes mund të klikoni në linkun më poshtë <small>(Kujdes: Keni vetëm 3 raste për të ndryshuar çmimin.) </small></p>';
                $message .= $weburl.'/ndrysho-shpalljen?post='.$public_id.'&id='.$post_identification;
                $message .= '<p>Nëse dëshironi që ta ndryshoni shpalljen tuaj atëher ju klikoni ne butonin "Ndrysho" dhe jepni parullën tuaj. '.$random_string.'</p>';
                $message .= '<br /><p>Për të larguar tërësisht shpalljen klikoni në linkun e mëposhtëm: <small>(Kujdes: Shpallja do të fshihet sapo të klikoni në link.) </small> </p>';
                $message .= $weburl.'/konfirmo-fshirjen?post='.$public_id.'&id='.$post_identification;
                 $new_url = add_query_arg( 'success', 1, get_permalink() );
                wp_redirect( $new_url, 303 );
                //dergojm konfirmimin ne emailin e postuestit
                ac_send_mail($post_id, $ac_email_postuesit, $message);
                //ridrejtojm pas postimit ne faqen kryesore
                    $ac_home = home_url();
                    //echo "<meta http-equiv='refresh' content='5;url=$ac_home' />"; exit;
                    //header("refresh:2;url=".$ac_home);
                    //exit();
            
            }//if($post_id)
            else {
               // $error[] .= "Shpallja nuk është shtuar.";
                $error[] .= "Të dhënat e njejta nuk mund të shtohen, përsëri.";
            }
        }//
        else {
           $error[] .= "Shpallja nuk është shtuar.";
        }
    } //if(isset($_POST['ac_shto_shpallje']
    else {
        $error[] .= "Gabim !";
    }
}//if 'POST' = $_SERVER['REQUEST_METHOD']
?>
<?php get_header(); ?>
<section class="listing-container">
    <div class="row">
        <div class="container">
            <div class="span12 listings">
                <?php
                    if(isset($error) && !empty($error)) {
                       
                    ?>
                     <script type="text/javascript">
                     $(document).ready(function(){
                        $(function() {
                            $.pnotify({
                                title: "Kanë ndodhur gabime !",
                                text: "<?php 
                                    foreach($error as $err) {

                                    echo '<ul><li>'.$err.'</li></ul>';
                                }
                                ?>",
                                styling: 'bootstrap',
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:6000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"error",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                     });
                    </script>
                    <?php
                    }
                    elseif($_GET['success'] == 1) {

                    ?>
                    <script type="text/javascript">
                     $(document).ready(function(){
                        $(function() {
                            $.pnotify({
                                title: "Të dhënat u shtuan me sukses",
                                text: "<br /><p>Pasi të miratohet, shpallja do të jetë në faqen tonë. Klikoni në linkun e dërguar në email-in e shënuar, për të larguar apo ndryshuar shpalljen.(Kujdes: Linkun në disa raste mund të gjeni edhe në junk mail !)</p>",
                                styling: 'bootstrap',
                                icon:"icon-ok icon-2x",
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:7000,
                                hide:true,
                                history: false,
                                animation:"fade",
                                animate_speed: "fast",
                                type:"success",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                        });
                    </script>
                    <?php
                    
                    }
                    else {
                    ?>
                    <script type="text/javascript">
                     $(document).ready(function(){
                        $(function() {
                            $.pnotify({
                                title: "",
                                text: "<?php  echo '<br /><h4>Plotësoni me kujdes të dhënat për të shtuar shpallje.</h4>'; ?>",
                                styling: 'bootstrap', 
                                icon: "icon-info-sign icon-2x",
                                closer_hover:true,
                                nonblock: false,
                                nonblock_opacity: 2,
                                sticker: false,
                                delay:3000,
                                hide:true,
                                history: false,
                                min_height:"150px",
                                animation:"fade",
                                animate_speed: "fast",
                                type:"info",
                                width:'500px',
                                remove:true,
                                before_open: function(pnotify) {
                                pnotify.css({
                                            "top": ($(window).height() / 2) - (pnotify.height() / 2),
                                            "left": ($(window).width() / 2) - (pnotify.width() / 2)
                                        });
                                    }
                                });
                            });
                        });
                    </script>  
                    <?php  
                    }
                ?>

                <form action="" method="POST" id="ac_forma_shto_shpallje" name="ac_form_shto_shpallje" enctype="multipart/form-data">
                    <div class="span8 leftZero">
                        <h4>Të dhënat e përgjithshme:</h4>
                        <div class="error" style="display:none;">
                          <span></span>.<br clear="all"/>
                        </div>
                        <div class="clearfix">
                            <div class="span4">
                                <label for="ac_shpallja_excerpt">Shpallje për:<small class="photoAddTitle" data-toggle="tooltip" title="Titulli i shpalljes nuk duhet të jetë më shume se 50 karaktere, duhet të përshkruaj thjesht automjetin, pa detaje të panevojshme."><i class="icon icon-question" ></i></small></label>
                                <input type="text" name="ac_shpallja_excerpt" id="ac_shpallja_excerpt" class="input required" maxlength="50"/>
                            </div>
                            
                            <div class="span4">
                                <label for="lloji">Lloji i automjetit:</label>
                                <?php echo ac_get_categories_dropdown('lloji', 'lloji', $lloji); ?>
                            </div>
                        </div>

                        <div class="clearfix select-agro" style="display:none;">
                            <div class="span3 leftZero">
                                <label for="ac_select_agro">Modeli i makinës bujqësore:</label>
                                <select name="ac_select_agro" id="ac_select_agro" class="postform"><option value="">---</option>
                                    <option value="AEBI">AEBI</option><option value="AGCO MASSEY FERGUSON">AGCO M. FERGUSON</option><option value="AGRIA">AGRIA</option><option value="AGRITEC">AGRITEC</option><option value="AGT">AGT</option><option value="ALF">ALF</option><option value="ALFA_LAVAL">ALFA_LAVAL</option><option value="AL_KO">AL_KO</option><option value="ALÖ">ALÖ</option><option value="AMAZONE">AMAZONE</option><option value="AS_MOTOR">AS_MOTOR</option><option value="AUTOWRAP">AUTOWRAP</option><option value="Atlas">Atlas</option><option value="BAAS">BAAS</option><option value="BAECHT">BAECHT</option><option value="BARIGELLI">BARIGELLI</option><option value="BAUER">BAUER</option><option value="BBG">BBG</option><option value="BECKER">BECKER</option><option value="BELARUS">BELARUS</option><option value="BERAL">BERAL</option><option value="BERGMANN">BERGMANN</option> <option value="BERTI">BERTI</option><option value="BLEINROTH">BLEINROTH</option><option value="BRIGGS&amp;STRATTON">BRIGGS&amp;STRATTON</option><option value="BRIX">BRIX</option><option value="BUCHER">BUCHER</option><option value="Bourgoin">Bourgoin</option><option value="Bourgoin">Bourgoin</option>
                                    <option value="CAPELLO">CAPELLO</option><option value="CASE_IH">CASE_IH</option><option value="CLAAS">CLAAS</option> <option value="CRAMER">CRAMER</option><option value="Caeb">Caeb</option><option value="DEUTZ_FAHR">DEUTZ_FAHR</option><option value="DIADEM">DIADEM</option><option value="DOLL">DOLL</option><option value="DUTZI">DUTZI</option><option value="EBERHARDT">EBERHARDT</option><option value="EICHER">EICHER</option><option value="FAHR">FAHR</option><option value="FEHRENBACH">FEHRENBACH</option><option value="FELLA">FELLA</option><option value="FENDT">FENDT</option><option value="FIATAGRI">FIATAGRI</option><option value="FLÖTZINGER">FLÖTZINGER</option><option value="Foton">Foton</option><option value="GALLAGHER">GALLAGHER</option><option value="GASPARDO">GASPARDO</option><option value="GASSNER">GASSNER</option><option value="GERINGHOFF">GERINGHOFF</option><option value="GOTHA">GOTHA</option><option value="GRASDORF">GRASDORF</option><option value="GRIESSER">GRIESSER</option><option value="GRUBER">GRUBER</option><option value="GUTBROD">GUTBROD</option><option value="Goldoni">Goldoni</option><option value="GÜLDNER">GÜLDNER</option><option value="HAGEDORN">HAGEDORN</option><option value="HARDI">HARDI</option><option value="HASSIA">HASSIA</option><option value="HEYWANG">HEYWANG</option><option value="HOLDER">HOLDER</option><option value="HONDA">HONDA</option>
                                    <option value="HORSCH">HORSCH</option><option value="HOWARD">HOWARD</option><option value="HUSQVARNA">HUSQVARNA</option><option value="Hanomag">Hanomag</option><option value="IGLAND">IGLAND</option><option value="IHC">IHC</option><option value="IMT">IMT</option><option value="ISEKI">ISEKI</option><option value="Iveco">Iveco</option>
                                    <option value="JF">JF</option><option value="JUNGHENRICH">JUNGHENRICH</option><option value="John Deere">John Deere</option><option value="KEMPER">KEMPER</option>
                                    <option value="KLEINE">KLEINE</option><option value="KRIEGER">KRIEGER</option><option value="KRÖGER">KRÖGER</option><option value="KUHN">KUHN</option> <option value="KVERNELAND">KVERNELAND</option><option value="Kramer">Kramer</option><option value="Krone">Krone</option><option value="Kubota">Kubota</option><option value="KÄRCHER">KÄRCHER</option><option value="KÖCKERLING">KÖCKERLING</option>
                                    <option value="KÖLA">KÖLA</option><option value="Kögel">Kögel</option><option value="LAMBORGHINI">LAMBORGHINI</option><option value="LANDSBERG">LANDSBERG</option><option value="LAVERDA">LAVERDA</option><option value="LELY">LELY</option><option value="LEMKEN">LEMKEN</option><option value="LEYLAND">LEYLAND</option><option value="LIFTER">LIFTER</option><option value="LIFTON">LIFTON</option><option value="LINDNER">LINDNER</option><option value="LOMBARDINI">LOMBARDINI</option><option value="Langendorf">Langendorf</option><option value="MAN">MAN</option><option value="MANITOU">MANITOU</option><option value="MASCHIO">MASCHIO</option><option value="MAYER">MAYER</option><option value="MC HALE">MC HALE</option><option value="MEELS">MEELS</option><option value="MENGELE">MENGELE</option><option value="MONOSEM">MONOSEM</option><option value="MORRA">MORRA</option><option value="MTD">MTD</option>
                                    <option value="MUH">MUH</option><option value="McCormick">McCormick</option><option value="MÖRTL">MÖRTL</option><option value="NEUERO">NEUERO</option><option value="NEW HOLLAND">NEW HOLLAND</option><option value="NIEMEYER">NIEMEYER</option><option value="NODET">NODET</option><option value="Naud">Naud</option><option value="Neuson">Neuson</option><option value="O&amp;K">O&amp;K</option><option value="Ostalo">Ostalo</option><option value="PEGORARO">PEGORARO</option><option value="PERKINS">PERKINS</option><option value="PLEINTINGER">PLEINTINGER</option><option value="PZ_VICON">PZ_VICON</option><option value="Porsche">Porsche</option><option value="PÖTTINGER">PÖTTINGER</option><option value="QUIVOGNE">QUIVOGNE</option><option value="RABE">RABE</option><option value="RAU">RAU</option><option value="REFORMWERKE WELS">REFORMWERKE WELS</option><option value="REGENT">REGENT</option><option value="RICONA">RICONA</option><option value="ROPA">ROPA</option><option value="ROTH">ROTH</option><option value="ROTOLAND">ROTOLAND</option><option value="Rapid">Rapid</option><option value="Reform">Reform</option><option value="Renault">Renault</option><option value="SAME">SAME</option><option value="SAMPO">SAMPO</option><option value="SAXONIA">SAXONIA</option><option value="SCHLÜTER">SCHLÜTER</option><option value="SCHMOTZER">SCHMOTZER</option><option value="SCHRIEVER">SCHRIEVER</option><option value="SCHUITEMAKER SR HOLLAN">SCHUITEMAKER SR HOLLAN</option><option value="SEKO">SEKO</option><option value="SILOKING">SILOKING</option><option value="SILOKING">SILOKING</option><option value="STEGSTEDT">STEGSTEDT</option><option value="STIGA">STIGA</option><option value="STIHL">STIHL</option><option value="STOLL">STOLL</option><option value="Sandtec">Sandtec</option><option value="Schaeff">Schaeff</option><option value="Schwarzmüller">Schwarzmüller</option><option value="Sileo">Sileo</option><option value="Steyr">Steyr</option><option value="TAARUP">TAARUP</option><option value="TAURUS">TAURUS</option><option value="TEBBE">TEBBE</option><option value="TECNOMA">TECNOMA</option><option value="TECUMSEH">TECUMSEH</option><option value="TIM">TIM</option><option value="TIMBERJACK">TIMBERJACK</option><option value="TOMA VINKOVIĆ">TOMA VINKOVIĆ</option><option value="TORNADO">TORNADO</option><option value="TORO">TORO</option><option value="TRUMAG">TRUMAG</option><option value="URSUS">URSUS</option><option value="VALMET">VALMET</option><option value="VICON">VICON</option><option value="VOGEL&amp;NOOT">VOGEL&amp;NOOT</option><option value="Volvo">Volvo</option><option value="WELGER">WELGER</option><option value="WEWELER">WEWELER</option><option value="WILMS">WILMS</option><option value="Weber">Weber</option><option value="ZETOR">ZETOR</option><option value="ZF">ZF</option><option value="ZUIDBERG">ZUIDBERG</option><option value="ZWEEGERS">ZWEEGERS</option><option value="Zmaj">Zmaj</option><option value="ÖVERUM">ÖVERUM</option>
                                </select>
                            </div>

                            <div class="span2 leftZero">
                                <label for="ac_agro_serie">Seria:</label>
                                <input type="text" name="ac_agro_serie" id="ac_agro_serie"/>
                            </div>

                            <div class="span3 leftZero">
                                <label for="ac_agro_year">Viti i prodhimit:</label>
                                <select name="ac_agro_year" id="ac_agro_year" class="postform"><option value="">---</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option></select>
                            </div>
                        </div><!-- /select-agro -->

                        <div class="clearfix select-build-truck" style="display:none;">
                            <div class="span3 leftZero">
                                <label for="ac_build_truck">Modeli:</label>
                                <select name="ac_select_build" id="ac_select_build" class="postform">
                                    <option value="">---</option><option value="Ammann">Ammann</option><option value="BMD">BMD</option><option value="Bobcat">Bobcat</option><option value="CASE">CASE</option><option value="Caterpillar">Caterpillar</option><option value="Dynapac">Dynapac</option><option value="FAI">FAI</option><option value="Fiat_Hitachi">Fiat_Hitachi</option><option value="Fiatallis">Fiatallis</option><option value="Fuchs">Fuchs</option><option value="Haulotte">Haulotte</option><option value="JCB">JCB</option><option value="Kaeser">Kaeser</option><option value="Komatsu">Komatsu</option><option value="Liebherr">Liebherr</option><option value="Linde">Linde</option><option value="MERLO">MERLO</option><option value="Montabert">Montabert</option><option value="Ostalo">Ostalo</option><option value="Putzmeister">Putzmeister</option><option value="Schaeff">Schaeff</option><option value="Terex">Terex</option><option value="Volvo">Volvo</option>
                                </select>
                            </div>

                            <div class="span2 leftZero">
                                <label for="ac_build_serie">Seria:</label>
                                <input type="text" name="ac_build_serie" id="ac_build_serie"/>
                            </div>
                            
                            <div class="span3 leftZero">
                                <label for="ac_build_year">Viti i prodhimit:</label>
                                <select name="ac_build_year" id="ac_build_year" class="postform"><option value="">---</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option></select>
                            </div>
                        </div><!-- select-build-truck -->

                        <div class="clearfix select-truck" style="display:none;">
                            <div class="span3 leftZero">
                                <label for="ac_truck_model">Modeli i kamionit</label>
                                <select name="ac_select_truck" id="ac_select_truck" class="postform"><option value="">---</option><option value="Citroen">Citroen</option><option value="DAF">DAF</option><option value="Dacia">Dacia</option><option value="Fap">Fap</option><option value="Fiat">Fiat</option><option value="Ford">Ford</option><option value="Hyundai">Hyundai</option><option value="Isuzu">Isuzu</option><option value="Iveco">Iveco</option><option value="Kia">Kia</option><option value="Krone">Krone</option><option value="MAN">MAN</option><option value="Mazda">Mazda</option><option value="Mercedes_Benz">Mercedes_Benz</option><option value="Mitsubishi">Mitsubishi</option><option value="Nissan">Nissan</option><option value="Opel">Opel</option><option value="Ostalo">Ostalo</option><option value="Peugeot">Peugeot</option><option value="Piaggio">Piaggio</option><option value="Renault">Renault</option><option value="Scania">Scania</option><option value="Steyr">Steyr</option><option value="Suzuki">Suzuki</option><option value="TAM">TAM</option><option value="Toyota">Toyota</option><option value="VW">VW</option><option value="Volvo">Volvo</option><option value="Zastava">Zastava</option><option value="Škoda">Škoda</option>
                                </select> 
                            </div>

                            <div class="span2 leftZero">
                                <label for="ac_truck_serie">Seria:</label>
                                <input type="text" name="ac_truck_serie" id="ac_truck_serie" style="height:100%; line-height:auto;" />
                            </div>

                            <div class="span3 leftZero">
                                <label for="ac_truck_year">Viti i prodhimit:</label>
                                <select name="ac_truck_year" id="ac_truck_year" class="postform"><option value="">---</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option></select>
                            </div>
                        </div><!-- /select-truck -->
                        <div class="clearfix select-motor" style="display:none;">
                            <div class="span3 leftZero">
                                <label for="ac_motor_model">Modeli i motorit:</label>
                                <select name="ac_motor_model" id="ac_motor_model" class="motor-make postform" tabindex="1">
                                    <option value="">Zgjedh prodhuesit (të gjithë)</option><option value="Aprilia">Aprilia</option><option value="Bmw">Bmw</option><option value="Buell">Buell</option><option value="Can-Am">Can-Am</option><option value="Ducati">Ducati</option><option value="Harley-Davidson">Harley-Davidson</option><option value="Honda">Honda</option><option value="Johnny Pag">Johnny Pag</option>
                                    <option value="Kawasaki">Kawasaki</option><option value="Ktm">Ktm</option><option value="Kymco">Kymco</option><option value="Suzuki">Suzuki</option><option value="Triumph|2321036">Triumph</option><option value="Victory|6772333">Victory</option><option value="Yamaha">Yamaha</option><option value="A.P.C. Motor Company|2315108">A.P.C. Motor</option><option value="Acm">Acm</option><option value="Aluma">Aluma</option><option value="American">American</option><option value="American Eagle">American Eagle</option><option value="American Ironhorse">American Ironhorse</option><option value="American Motorcycle Company">American Motorcycle Co</option><option value="American Quantum Cycles">American Quantum</option><option value="Apollo Chopper">Apollo Chopper</option><option value="Aprilia">Aprilia</option><option value="Arctic Cat">Arctic Cat</option><option value="Ariel">Ariel</option><option value="Arlen Ness">Arlen Ness</option><option value="Aspt">Aspt</option><option value="Atk">Atk</option><option value="Baja Motorsports">Baja Motorsports</option><option value="Bajaj">Bajaj</option><option value="Bamx">Bamx</option><option value="Benelli">Benelli</option><option value="Bennche">Bennche</option><option value="Beta">Beta</option><option value="Big Bear Choppers">Big Bear Choppers</option><option value="Big Dog Motorcycles">Big Dog Motorcycles</option><option value="Big Inch Bikes">Big Inch Bikes</option><option value="Bimota">Bimota</option><option value="Blata">Blata</option><option value="Bmc">Bmc</option><option value="Bms">Bms</option><option value="Bmw">Bmw</option><option value="Boss Hoss">Boss Hoss</option><option value="Bourget">Bourget</option><option value="Brammo ">Brammo </option><option value="Bridgestone">Bridgestone</option><option value="Bsa">Bsa</option><option value="Buell|2315776">Buell</option><option value="Cagiva">Cagiva</option><option value="California Motorcycle Co">California Motorcycle Co</option><option value="California Side Car">California Side Car</option><option value="Campagna">Campagna</option><option value="Can-Am">Can-Am</option><option value="Carefree Custom Cycles">Carefree Custom Cycles</option><option value="Cfmoto">Cfmoto</option><option value="Champion Sidecar">Champion Sidecar</option><option value="Champion Trike Conversion">Champion Trike Con</option><option value="Christini">Christini</option><option value="Cleveland Cyclewerks">Cleveland Cyclewerks</option><option value="Cobra">Cobra</option><option value="Condor">Condor</option><option value="Confederate Motorcycles">Confederate Motorcycles</option><option value="Coolster">Coolster</option><option value="Covington Cycle City">Covington Cycle City</option><option value="Daelim">Daelim</option><option value="Demon (dmc)">Demon (dmc)</option><option value="Dft">Dft</option><option value="Diablo">Diablo</option><option value="Diamo|185451976">Diamo</option><option value="Droptail">Droptail</option><option value="Ducati">Ducati</option><option value="E-Ton">E-Ton</option><option value="Ebr|765322960">Ebr</option><option value="Ecocycle">Ecocycle</option><option value="Eddie Trotta">Eddie Trotta</option><option value="Enfield">Enfield</option><option value="Evolution Bike Works|423764452">Evolution Bike Works</option><option value="Evolve">Evolve</option><option value="Excelsior-Henderson">Excelsior-Henderson</option><option value="Exotix Cycle">Exotix Cycle</option><option value="Flyrite Choppers">Flyrite Choppers</option><option value="Fn">Fn</option><option value="Garelli">Garelli</option><option value="Gas Gas">Gas Gas</option><option value="Gem">Gem</option><option value="Hannigan">Hannigan</option><option value="Hard-Bikes">Hard-Bikes</option><option value="Hardcore Choppers">Hardcore Choppers</option><option value="Harley-Davidson">Harley-Davidson</option><option value="Haulmark">Haulmark</option><option value="Hercules">Hercules</option><option value="Hilltop Custom Cycle">Hilltop Custom Cycle</option><option value="Hodaka">Hodaka</option><option value="Honda">Honda</option><option value="Horex">Horex</option><option value="Husaberg">Husaberg</option><option value="Husqvarna">Husqvarna</option><option value="Hyosung">Hyosung</option><option value="Ice Bear">Ice Bear</option><option value="Independence|29519182">Independence</option><option value="Indian">Indian</option><option value="Intrepid">Intrepid</option><option value="Jawa Cz">Jawa Cz</option><option value="Jcl">Jcl</option><option value="Jianshe">Jianshe</option><option value="Jincheng">Jincheng</option><option value="Johnny Pag">Johnny Pag</option><option value="Jonway">Jonway</option><option value="Joyner">Joyner</option><option value="Kaotic Customs">Kaotic Customs</option><option value="Karavan">Karavan</option><option value="Kawasaki">Kawasaki</option><option value="Keeway">Keeway</option><option value="Knievel">Knievel</option><option value="Kodiak">Kodiak</option><option value="Kraft/Tech">Kraft/Tech</option><option value="Ktm">Ktm</option><option value="Kymco">Kymco</option><option value="Lambretta">Lambretta</option><option value="Lance">Lance</option><option value="Lehman Trike">Lehman Trike</option><option value="Lehman Trikes/Honda">Lehman Trikes/Honda</option><option value="Lehman Trikes/Kawasaki">Lehman Trikes/Kawasaki</option><option value="Linhai">Linhai</option><option value="Lucky 7 Choppers">Lucky 7 Choppers</option><option value="Madami">Madami</option><option value="Maico">Maico</option><option value="Malaguti">Malaguti</option><option value="Marusho">Marusho</option><option value="Matchless">Matchless</option><option value="Mid-West Choppers">Mid-West Choppers</option><option value="Moto Guzzi">Moto Guzzi</option><option value="Motobravo">Motobravo</option><option value="Motofino">Motofino</option><option value="Motor Trike">Motor Trike</option><option value="Motor Trike Conversion">Motor Trike Conversion</option><option value="Motus">Motus</option><option value="Mustang">Mustang</option><option value="Mv Agusta">Mv Agusta</option><option value="Mz">Mz</option><option value="Norton">Norton</option><option value="Nsu">Nsu</option><option value="Orange County Choppers">Orange County Choppers</option><option value="Oset">Oset</option><option value="Ossa">Ossa</option><option value="Other">Other</option><option value="Paul Yaffe">Paul Yaffe</option><option value="Peace Sports">Peace Sports</option><option value="Peirspeed">Peirspeed</option><option value="Penton">Penton</option><option value="Piaggio">Piaggio</option><option value="Pitster Pro|456771304">Pitster Pro</option><option value="Pocket Bike">Pocket Bike</option><option value="Polaris">Polaris</option><option value="Polini">Polini</option><option value="Precision Cycle Works">Precision Cycle Works</option><option value="Pro Street">Pro Street</option><option value="Pro-One">Pro-One</option><option value="Proper Chopper">Proper Chopper</option><option value="Puch">Puch</option><option value="Qingqi">Qingqi</option><option value="Qlink">Qlink</option><option value="Red Horse|14736725">Red Horse</option><option value="Redcat">Redcat</option><option value="Redneck Engineering">Redneck Engineering</option><option value="Redstreak Motors">Redstreak Motors</option><option value="Ridley">Ridley</option><option value="Road Smith">Road Smith</option><option value="Roketa|374244250">Roketa</option><option value="Royal Enfield">Royal Enfield</option><option value="Royal Ryder">Royal Ryder</option><option value="Sachs">Sachs</option><option value="Santee">Santee</option><option value="Saxon">Saxon</option><option value="Sdg">Sdg</option><option value="Sea Doo">Sea Doo</option><option value="Sears">Sears</option><option value="California Scooter Co.">California Scooter Co.</option><option value="Schwinn Scooters">Schwinn Scooters</option><option value="Genuine Scooter Company">Genuine Scooter Co.</option><option value="Segway">Segway</option><option value="Shenke Motor">Shenke Motor</option><option value="Sherco">Sherco</option><option value="Shoreland'R">ShorelandR</option><option value="Spcns">Spcns</option><option value="Spencer Bowman">Spencer Bowman</option><option value="Ssr Motorsports">Ssr Motorsports</option><option value="Star Motorcycles">Star Motorcycles</option><option value="Streamline">Streamline</option><option value="Suckerpunch Sallys">Suckerpunch Sallys</option><option value="Sunbeam">Sunbeam</option><option value="Surgical-Steeds">Surgical-Steeds</option><option value="Suzuki">Suzuki</option><option value="Swift|4989763">Swift</option><option value="Sym">Sym</option><option value="Taotao">Taotao</option><option value="Tgb">Tgb</option><option value="The Trike Shop">The Trike Shop</option><option value="Thoroughbred Motorsports">Thoroughbred Moto.</option><option value="Thunder Cycle Designs">Thunder Cycle Designs</option><option value="Thunder Mountain Custom">Thunder Mountain</option><option value="Titan Motorcycle Co.">Titan Motorcycle Co.</option><option value="Tomberlin">Tomberlin</option><option value="Tomos">Tomos</option><option value="Triumph">Triumph</option><option value="Ultima">Ultima</option><option value="Ultra Cycle">Ultra Cycle</option><option value="United Motors">United Motors</option><option value="Universal">Universal</option><option value="Ural">Ural</option><option value="Vectrix">Vectrix</option><option value="Vengeance">Vengeance</option><option value="Vento|132855914">Vento</option><option value="Vespa">Vespa</option><option value="Victoria">Victoria</option><option value="Victory|6772333">Victory</option><option value="Viper">Viper</option><option value="Von Dutch Kustom">Von Dutch Kustom</option><option value="Voyager">Voyager</option><option value="War Eagle Customs">War Eagle Customs</option><option value="West Coast Chopper">West Coast Chopper</option><option value="Whizzer">Whizzer</option><option value="Wild West">Wild West</option><option value="Wildfire">Wildfire</option><option value="Xingyue">Xingyue</option><option value="Xtreme">Xtreme</option><option value="Yama Buggy">Yama Buggy</option><option value="Yamaha">Yamaha</option><option value="Zero Motorcycles">Zero Motorcycles</option>
                                    <option value="Zhejiang Xingyue">Zhejiang Xingyue</option>
                                <option value="Zhng">Zhng</option><option value="Zieman">Zieman</option>
                                <option value="Znen">Znen</option><option value="Zongshen">Zongshen</option>
                                </select>
                            </div>

                            <div class="span1 leftZero">
                                <label for="ac_motor_serie">Seria:</label>
                                <input type="text" name="ac_motor_serie" id="ac_motor_serie"/>
                            </div>

                            <div class="span2 leftZero">
                                <label for="ac_motor_size">Motori(cc):</label>
                                <select id="ac_motor_size" name="ac_motor_size" class="postform">
                                <option class="" value=""> --- </option><option class="" value="0cc">0cc</option><option class="" value="50cc">50cc</option><option class="" value="125cc">125cc</option><option class="" value="200cc">200cc</option><option class="" value="300cc">300cc </option><option class="" value="400cc">400cc</option><option class="" value="500cc">500cc</option><option class="" value="600cc">600cc</option><option class="" value="700cc">700cc</option><option class="" value="800cc">800cc</option><option class="" value="900cc">900cc</option><option class="" value="1000cc">1000cc</option><option class="" value="1100cc">1100cc</option><option class="" value="1200cc">1200cc</option><option class="" value="1300cc">1300cc</option><option class="" value="1400cc">1400cc</option><option class="" value="1600cc">1600cc</option>
                                    </select>
                            </div>

                            <div class="span2 leftZero">
                                <label for="ac_motor_year">Viti i prodhimit:</label>
                                <select name="ac_motor_year" id="ac_motor_year" class="postform"><option value="">---</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option></select>
                            </div>
                        </div><!-- /select-motor -->
                        <div class="clearfix select-cars">
                         <div class="span3 leftZero">
                                <label for="select_modeli">Modeli:</label>
                                <select name="select_modeli" id="select_modeli"  class="car_select" style="width:35%">
                                <option selected="selected" value="">---</option>
                                <?php
                                    global $wpdb;
                                    $results = $wpdb->get_results("SELECT * FROM `wp_auto_list`");
                                    foreach($results as $res) {
                                        echo '<option value="'.$res->model.'" data-id="'.$res->id.'">'.$res->model.'</option>';
                                    }
                                ?>
                            </select>
                            </div>
    
                            <div class="span3 leftZero">
                                <label for="select_serie">Seria: <span id="image_loader" class="none"><img src="<?php echo THEMEROOT; ?>/images/468.gif" /></span></label>
                                <select name="select_serie" id="select_serie" class="car_select">
                                <option selected="selected" value="">---</option>
                                     </select>
                            </div>
                            
                            <div class="span2 leftZero">
                                <label for="select_year">Viti prodhimit :</label>
                                <select name="select_year" id="select_year" class="postform">
                                <option value="">---</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option>
                                </select>
                            </div>   

                        </div>

                        <div class="clearfix">
                            <div class="span4">
                                <label for="lenda_djegese">Lënda djegëse:</label>
                                <?php echo ac_get_categories_dropdown('lenda_djegese', 'lenda_djegese', $lenda_djegese); ?>
                            </div>

                            <div class="span4">
                                <label for="ac_kubikazhi">Kubikazhi:</label>
                                <select name="ac_kubikazhi" id="ac_kubikazhi" class="postform ac_kubik">
                                    <option value="50">50</option>
                                    <option value="125">125</option>
                                    <option value="250">250</option>
                                    <option value="400">400</option>
                                    <option value="600">600</option>
                                    <option value="900">900</option>
                                    <option value="1000">1000</option>
                                    <option value="1100">1100</option>
                                    <option value="1200">1200</option>
                                    <option value="1300">1300</option>
                                    <option value="1400">1400</option>
                                    <option value="1500">1500</option>
                                    <option value="1600">1600</option>
                                    <option value="1700">1700</option>
                                    <option value="1800">1800</option>
                                    <option value="1900">1900</option>
                                    <option value="2000">2000</option>
                                    <option value="2100">2100</option>
                                    <option value="2200">2200</option>
                                    <option value="2300">2300</option>
                                    <option value="2400">2400</option>
                                    <option value="2500">2500</option>
                                    <option value="2600">2600</option>
                                    <option value="2700">2700</option>
                                    <option value="2800">2800</option>
                                    <option value="2800">2900</option>
                                    <option value="3000">3000</option>
                                    <option value="3000">3100</option>
                                    <option value="3500">3500</option>
                                    <option value="4000">4000</option>
                                    <option value="5000">+4000</option>
                                </select>
                            </div>   
                        </div>


                        <div class="clearfix">
                            <div class="span4">
                                <label for="transmisioni">Tipi i transmisionit: </label>
                                <?php echo ac_get_categories_dropdown('transmisioni', 'transmisioni', $transmisioni); ?>
                            </div>

                            <div class="span4">
                                <label for="ac_km_kaluara">Kilometra të kaluara (km): <small class="photoAddTitle" data-toggle="tooltip" title="Shëno vetëm numrin pa hapësira apo presje dhjetore."><i class="icon icon-question" ></i></small></label>
                                <input type="text" id="ac_km_kaluara" name="ac_km_kaluara" class="input">
                            </div>
                        </div>

                        <div class="clearfix">
                            <div class="span4">
                                <label for="ac_cmimi_auto">Çmimi:<small class="photoAddTitle" data-toggle="tooltip" title="Shënoni vetëm numrin për çmimin e automjetit, pa hapësira presje dhjetore apo shenjë valute."><i class="icon icon-question" ></i></small></label>
                                <input type="text" id="ac_cmimi_auto" name="ac_cmimi_auto">
                            </div>
                            <div class="span4">
                                <label>Çmimi është: <small class="photoAddTitle" data-toggle="tooltip" title="Nëse cmimi është i diskutueshëm, apo bëni ndërrim zgjedhni njërin nga opsionet e mëposhtme. Nëse çmimi nuk ndryshon zgjedhni opsionin çmimi fiks."><i class="icon icon-question" ></i></small></label>
                                <ul class="inputs-list leftZero">
                                    <li> 
                                        <input type="checkbox" name="ac_checkbox_cmimi[0]" id="ac_checkbox_cmimi" class="first_val check_val" value="0" style="float:left;">
                                        Me marrëveshje
                                    </li>
                                    <li>
                                        <input type="checkbox" name="ac_checkbox_cmimi[1]" id="ac_checkbox_cmimi" class="second_val check_val" value="1" style="float:left;">
                                        Çmimi është fiks
                                    </li>
                                    <li>
                                        <input type="checkbox" name="ac_checkbox_cmimi[2]" id="ac_checkbox_cmimi" class="third_val check_val" value="2" style="float:right;">
                                        Bëj ndërrim
                                    </li>
                                </ul>
                            </div><!-- /span4 -->


                        </div>
                    </div><!-- /span 8 -->

                    <div class="span4 contact_div">
                        <h4>Të dhënat për kontakt:</h4>
                        <div class="span4">
                            <label for="ac_pershkrimi">Përshkrimi i shpalljes:<small class="photoAddTitle" data-toggle="tooltip" title="Shënoni përshkrimin për automjetin, keni në dispozicion 350 karaktere. Përdorni me kujdes dhe shënoni vetëm të dhënat me rëndësi për automjetin. "><i class="icon icon-question" ></i></small></label>
                            <textarea name="ac_pershkrimi" id="ac_pershkrimi" cols="30" rows="6" maxlength="500"></textarea><br />
                        </div>
                        <div class="span4">
                            <label for="lokacioni">Lokacioni:</label>
                            <?php echo ac_get_categories_dropdown('lokacioni', 'lokacioni', $lokacioni); ?>
                        </div>
                        <div class="span4">
                            <label for="ac_emri_postuesit">Emri:</label>
                            <input type="text" id="ac_emri_postuesit" class="required" maxlength="50" name="ac_emri_postuesit" >
                        </div>

                        <div class="span4">
                            <label for="ac_email_postuesit">Email:<small class="photoAddTitle" data-toggle="tooltip" title="Shënoni një email valide në të cilen keni qasje."><i class="icon icon-question" ></i></small></label>
                            <input type="email" id="ac_email_postuesit" class="required email" maxlength="50" name="ac_email_postuesit">
                        </div>

                        <div class="span4">
                            <label for="ac_nr_postuesit">Numri i telefonit:</label>
                            <input type="text" id="ac_nr_postuesit" class="phone" maxlength="13" name="ac_nr_postuesit" >
                        </div>

                    </div><!-- /span 4 -->

                    <div class="span4 leftZero">
                        <label for="ac_parulla">Parulla e shpalljes:<small class="photoAddTitle" data-toggle="tooltip" title="Shënoni një parullë të cilë do ta përdorni për të ndryshuar shpalljen."><i class="icon icon-question" ></i></small></label></label>
                        <input type="text" id="ac_parulla" name="ac_parulla" />
                    </div><!-- / parulla -->

                    <div class="span12 leftZero">
                        <h4>Shto fotot e automjetit <small class="photoAddTitle" data-toggle="tooltip" title="Kujdes: Foto e fundit është ajo që shfaqet si foto e shpalljes. Zgjedhni me kujdes fotot, ato duhet të shfaqin mirë automjetin, nga të gjitha anët duke përfshirë edhe pamjen e mbrendshme të tij."><i class="icon icon-question" ></i></small></h4>
                        <ul class="list-files leftZero">
                            <li>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-preview thumbnail"></div>
                                  <div>
                                    <span class="btn btn-inverse btn-file"><span class="fileupload-new"><i class="icon icon-white icon-upload"></i>Shto foto</span><span class="fileupload-exists"><i class="icon-file-alt  icon-white"></i>Ndrysho</span><input type="file" name="ac_foto1" id="ac_foto1"></span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-remove icon-white"></i>Largo </a>
                                  </div>
                                </div>
                            </li>

                            <li>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-preview thumbnail"></div>
                                  <div>
                                    <span class="btn btn-inverse btn-file"><span class="fileupload-new"><i class="icon icon-white icon-upload"></i>Shto foto</span><span class="fileupload-exists"><i class="icon-file-alt  icon-white"></i>Ndrysho</span><input type="file" name="ac_foto2" id="ac_foto2"></span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-remove icon-white"></i>Largo </a>
                                  </div>
                                </div>   
                            </li>

                            <li>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-preview thumbnail"></div>
                                  <div>
                                    <span class="btn btn-inverse btn-file"><span class="fileupload-new"><i class="icon icon-white icon-upload"></i>Shto foto</span><span class="fileupload-exists"><i class="icon-file-alt  icon-white"></i>Ndrysho</span><input type="file" name="ac_foto3" id="ac_foto3"></span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-remove icon-white"></i>Largo </a>
                                  </div>
                                </div>
                            </li>

                            <li>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                  <div class="fileupload-preview thumbnail"></div>
                                  <div>
                                    <span class="btn btn-inverse btn-file"><span class="fileupload-new"><i class="icon icon-white icon-upload"></i>Shto foto</span><span class="fileupload-exists"><i class="icon-file-alt  icon-white"></i>Ndrysho</span><input type="file" name="ac_foto4" id="ac_foto4"></span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-remove icon-white"></i>Largo </a>
                                  </div>
                                </div>
                            </li>
                        </ul>
                    </div><!-- /span12 uploader -->
            </div><!-- /listings -->
            <div class="span12">
                 <input type="submit" name="ac_shto_shpallje" id="ac_shto_shpallje" value="Shto shpallje" class="btn btn-large btn-succes main-green frmBtn ignore btnReplace" data-label="Shto shpallje"/>
                <input type="hidden" name="ac_token" id="ac_token" value="<?php echo isset($submit_token) ?  $submit_token :  ''; ?>" class="ignore" />
                 <input type="hidden" name="ac_action" id="ac_action" value="automjete" class="ignore"/>
            <input type="hidden" name="ac_ruaj_shpallje_nonce" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>" class="ignore"/>
            </div>
            </form>
        </div><!-- /container -->
    </div><!-- /row -->
</section><!-- /listing-container -->
<?php
//funksionet ndihmese, duhet organizuar kodi, funksionet te barten me nje klase !

/**
* Funksioni i cili kthen kodin 32 karaktere.
* @param type @length 
* @return type character
*/
function ac_generate_random_string($length = '12') {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$random_string = '';
	for($i = 0; $i < $length; $i++) {
		$random_string .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $random_string;
}

/**
 * Funksioni kthen kategorite per taxonomite perkatese.
 * @param type $taxonomy
 * @param type $name
 * @param type $selected
 * @return type
 */
function ac_get_categories_dropdown($taxonomy, $name, $selected) {
    return wp_dropdown_categories(array(
        'taxonomy' => $taxonomy,
        'name' => $name,
        'selected' => $selected,
        'class' => 'postform ignore',
        'hide_empty' => 0,
        'depth' => 1,
        'parent' => 0,
        // 'walker' => new SH_Walker_TaxonomyDropdown(),
        'echo' => 0
    ));
}

/**
 * Funksioni i cili do te shtoje imazhet per secilin post.
 * @param type $file_handler
 * @param type $post_id
 * @param type $setthumb
 * @return type
 */
function ac_insert_attachment($file_handler, $post_id, $setthumb = 'false') {
    if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK)
        __return_false();

    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id = media_handle_upload($file_handler, $post_id);
    if ($setthumb) {
        update_post_meta($post_id, '_thumbnail_id', $attach_id);
    }

    update_post_meta($post_id, $file_handler, $attach_id);

    return $attach_id;
}

//$file = $_FILES['file_name']
?>
<?php get_footer(); ?>