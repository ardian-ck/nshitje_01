<?php
/**
 * Template Name: Ruaj Shpallje
 */
?>

<?php get_header(); ?>
<section class="favoriteposts_container">
  <div class="container">
    <div class="row">
      <div class="span12">
       <div id="fav_posts" class="clearfix"><?php wpfp_list_favorite_posts(); ?></div>
       <div class="clearfix"></div>
      </div>
    </div><!-- /row -->
  </div><!-- /.container -->
</section>
<?php get_footer(); ?>