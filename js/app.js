jQuery(window).load(function(){
  $('#status').fadeOut();
  $('#preloader').delay(250).fadeOut('slow');
        $('.views #stats').addClass('label');
  function imgLoaded(img){  
    $(img).parent().addClass('loaded');
  }
  $('span#image_loader').hide();

});

jQuery(document).ready(function($){
  $('.photoAddTitle').tooltip({
    placement: 'right'
  });

  $('a.gallery').colorbox({ rel:"gallery", transition:"fade", speed: '450'});

      $('.flexslider').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        prevText: "",
    nextText: "",
        start: function(slider){
          $('body').removeClass('loading');
        }
        });
  $('input:checkbox, input:radio').iCheck({
    checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green'
  });

  $.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z!ëçÇË\s]+$/);
        },"Lejohen vetëm shkrojna.");

  $("form#ac_forma_shto_shpallje").validate({
    invalidHandler: function(e, validator) {
      var errors = validator.numberOfInvalids();
      if(errors) {
        var message = errors == 1 ? 'Keni harruar të shtoni 1 fushë me të dhëna (fusha e harruar është hijezuar).' : 'Keni harruar të plotësoni ' + errors + ' fusha (fushat e harruara janë të hijezuara). ';
        $('div.error span').html(message);
        $('div.error').show();
      }
      else {
        $('div.error').hide();
      }
    },
    onkeyup: false,
    ignore: ".ignore",
    rules: {
      ac_shpallja_excerpt: {
        required:true
      },
      ac_cmimi_auto: {
        required: true,
        number: true
      },
      ac_emri_postuesit: {
        required:true,
        alpha: true,
        minlength:2
      },
      ac_km_kaluara: {
        required:true,
        number:true
      }
    },
    messages: {
      ac_shpallja_excerpt: {
        required:"Shënoni titullin e shpalljes."
      },
      ac_email_postuesit: {
        required:"Shënoni email.",
        email: "Ju lutem shënoni një email valide, në të cilën keni çasje."
      },
      ac_cmimi_auto: {
        required:"Shënoni çmimin.",
        number: "Çmimi duhet të jetë numër, pa presje. Shembull: 12000.."
      },
      ac_km_kaluara: {
        required:"Shënoni kilometrat e kaluara.",
        number: "Duhet të jetë numër, pa presje."
      },
      ac_emri_postuesit: {
        required:"Shënoni emrin.",
        alpha:"Emri duhet të përmbajë vetëm shkronja.",
        minlegth:"Më shumë se 2 shkronja."
      }
    }
  });

  $('#ac_nr_postuesit').mask("(099) 999-999");

  
  $('ul.news-scroller').liScroll({
    travelocity: 0.07
  });
  
  $('select#lloji').minimalect({
      onchange: function(text, value) {
        //console.log(text);
        if(text == '183') {
          $('select#ac_kubikazhi').prop('disabled', true);
          $('.select-cars').fadeOut('fast');
          $('.select-truck').fadeOut('fast');
          $('.select-agro').fadeOut('fast');
          $('.select-build-truck').fadeOut('fast');
          $('.select-motor').fadeIn('slow');
        }
        else if(text == '181') {
          $('.select-cars').fadeOut('fast');
          $('.select-truck').fadeOut('fast');
          $('.select-motor').fadeOut('fast');
          $('.select-build-truck').fadeOut('fast');
          $('.select-agro').fadeIn('slow');
        }
        else if(text == '182') {
          $('.select-cars').fadeOut('fast');
          $('.select-truck').fadeOut('fast');
          $('.select-motor').fadeOut('fast');
          $('.select-agro').fadeOut('fast');
          $('.select-build-truck').fadeIn('slow');
        }
        else if(text == '184') {
          $('.select-motor').fadeOut('fast');
          $('.select-cars').fadeOut('fast');
          $('.select-agro').fadeOut('fast');
          $('.select-build-truck').fadeOut('fast');
          $('.select-truck').fadeIn('slow');
        }
        else if(text == '2') {
          $('select#ac_kubikazhi').prop('disabled', false);
          $('.select-motor').fadeOut('fast');
          $('.select-truck').fadeOut('fast');
          $('.select-agro').fadeOut('fast');
          $('.select-build-truck').fadeOut('fast');
          $('.select-cars').fadeIn('slow');
        }
        else {
        }
      }
  });
    
  $('select#select_modeli').minimalect({
      onchange:function(value, text) {
        var model_id = value;
        //console.log(model_id);
  
        $.ajax({
          type:"POST",
          url:main_ajax.ajaxurl,
          data: {action:"get_serie_init", model_id : model_id },
          beforeSend: function() {
            $('span#image_loader').fadeIn();
          },
          
          complete: function() {
            $('span#image_loader').fadeOut();
          },
          
          success:function(response) {
            $('select#select_serie').html(response);
          }
        });
      }
  });

  $('.postform').minimalect();

  $('.minict_wrapper input').addClass("ignore");

  $('.fileupload').fileupload();

  $('.check_val').on('ifChecked', function(){
    var value = $(this).val();
    //console.log(value);
    if(value == 1) {
      $('.first_val').iCheck('disable');
      $('.first_val').iCheck('uncheck');
    }
    else if(value == 0 ) {
      $('.second_val').iCheck('disable');
      $('.second_val').iCheck('uncheck');
    }
    else if(value == 2) {
      $('.first_val').iCheck('enable');
      $('.second_val').iCheck('enable');
    }
    else {
      $('.first_val').iCheck('enable');
      $('.third').iCheck('enable');
    }
  });

  $('#ac_shto_shpallje').on('click', function(e){
    var foto = [];
    foto.push(($('#ac_foto1').val() !== '') ? $('#ac_foto1').val() : '');
    foto.push(($('#ac_foto2').val() !== '') ? $('#ac_foto2').val() : '');
    foto.push(($('#ac_foto3').val() !== '') ? $('#ac_foto3').val() : '');
    foto.push(($('#ac_foto4').val() !== '') ? $('#ac_foto4').val() : '');

    var test = check_unique(foto);

    if(test) {
    $.pnotify({
        title: "Kanë ndodhur gabime !",
        text: "Nuk mund te shtoni te njejten foto.",
        styling: 'bootstrap',
        closer_hover:true,
        nonblock: false,
        nonblock_opacity: 2,
        sticker: false,
        delay:6000,
        hide:true,
        history: false,
        animation:"fade",
        animate_speed: "fast",
        type:"warning",
        width:'400px',
        remove:true,
        before_open: function(pnotify) {
        pnotify.css({
                    "top": ($(window).height() / 2) - (pnotify.height() / 2),
                    "left": ($(window).width() / 2) - (pnotify.width() / 2)
                });
            }
      });
      e.preventDefault();
      return;
    }
  });
  
  function check_unique(arr) {
    var i, j, n;
    n = arr.length;
    for(i = 0; i < n; i++) {
      for(j = i + 1; j < n; j++) {
        if(arr[i] == arr[j] && arr[i] != '' && arr[j] !== '') {
          return true;
        }
      }
    }
    return false;
  }

  $('#Parks').mixitup({
    layoutMode: 'list', // Start in list mode (display: block) by default
    listClass: 'list', // Container class for when in list mode
    gridClass: 'grid', // Container class for when in grid mode
    effects: ['fade','blur'], // List of effects 
    listEffects: ['fade','rotateX'] // List of effects ONLY for list mode
  });

  $('.area').children().removeClass('all_price');
  $('#ToList').on('click',function(){
    $('.button').removeClass('active');
    $('.area').children().removeClass('all_price');
    $('div.main-info').css({'display' : 'block'});
    $('span.badge').css({'display' : 'inline-block' });
    $(this).addClass('active');
    $('#Parks').mixitup('toList');
  });

  $('#ToGrid').on('click',function(){
    $('.button').removeClass('active');
    $(this).addClass('active');
    $('div.main-info, span.badge').css({'display' : 'none'});
    $('.area').children().addClass('all_price');
    $('#Parks').mixitup('toGrid');
  });

  var $filters = $('#Filters').find('li'),
    dimensions = {
      region: 'all', // Create string for first dimension
      recreation: 'all' // Create string for second dimension
    };

  $filters.on('click',function(){
    var $t = $(this),
      dimension = $t.attr('data-dimension'),
      filter = $t.attr('data-filter'),
      filterString = dimensions[dimension];
      
    if(filter == 'all'){

      if(!$t.hasClass('active')){

        $t.addClass('active').siblings().removeClass('active');

        filterString = 'all'; 
      } else {

        $t.removeClass('active');

        filterString = '';
      }
    } else {

      $t.siblings('[data-filter="all"]').removeClass('active');

      filterString = filterString.replace('all','');
      if(!$t.hasClass('active')){
        $t.addClass('active');
        filterString = filterString == '' ? filter : filterString+' '+filter;
      } else {
        $t.removeClass('active');
        var re = new RegExp('(\\s|^)'+filter);
        filterString = filterString.replace(re,'');
      };
    };
    
    dimensions[dimension] = filterString;
    
    console.info('dimension 1: '+dimensions.region);
    console.info('dimension 2: '+dimensions.recreation);
 
    $('#Parks').mixitup('filter',[dimensions.region, dimensions.recreation])      
  });

  //ajax change post
  $('#small_loader').hide();
  $('#save_button').hide();

  $('#frm_change_post_submit').on('click', function(e) {
    var email_post = $('#user_post_email').val(),
        post_id    = $('#content > article').data('postid'),
        code_post  = $('#user_post_code').val();

    var error; 

    if(email_post == '') {
      $('#user_post_email').addClass('error_input');
      error = true;
    } else if(!is_valid_email_address(email_post)) {
      $('#user_post_email').addClass('error_input');
      error = true;
    } else {
      $('#user_post_email').removeClass('error_input');
      error = false;
    }

    if(code_post == '') {
      $('#user_post_code').addClass('error_input');
      error = true;
    } else {
      $('#user_post_code').removeClass('error_input');
    }

    var user_data = {
      'action' : 'change_user_post',
      'user_email' : email_post,
      'user_code' : code_post,
      'post_id' : post_id,
      'nonce' : main_ajax.nonce
    };

    if(error == false) {
      
      /*$.post(main_ajax.ajaxurl, user_data, function(response) {
        alert('Got this from ajax request ' + response);
      }); */
      $.ajax({
        type: "POST",
        url: main_ajax.ajaxurl,
        data: user_data,
        dataType: "json",
        beforeSend: function() {
          $('span#small_loader').fadeIn();
        },
        success: function(response) {
          $('span#small_loader').fadeOut('fast');
          if(response.flag == true) {
            //show message
            $('#ajax_message').removeClass('alert-new-warning');
            $('#ajax_message').addClass('alert-new-success').html("Klikoni fushat që dëshironi të ndryshoni.");
            //hide button
            $('#change_button').hide();
            //hide modal
            setTimeout(function() {
              $('.close-modal').trigger('click');
            }, 3000);

            //show save button
            $('#save_button').show();
            //define inline editable fields.
            var new_data = {
              'action'      : 'update_user_data',
              'title'   : '',
              'price'   : '',
              'content' : '',
              'post_id'     : post_id
            };

            $('#post_title').editable({
              mode: 'inline',
              type: 'text',
              pk: 1,
              title: "Shënoni emrin e shpalljes.",
              success:function(response, newValue) {
                $(this).html(newValue);
                new_data.title = newValue;
              }
            });

            $('.price_badge').editable({
              mode: "inline",
              type: "text",
              pk: 1,
              title: "Shënoni çmimin.",
              success: function(response, newValue) {
                $(this).html(newValue);
                new_data.price = newValue;
              }
            });

            $('.main_content').editable({
              mode: "inline",
              type: "textarea",
              pk: 1,
              title: "Ndrysho shpalljen.",
              success: function(response, newValue) {
                $(this).html(newValue);
                new_data.content = newValue;
              }
            });

           
            $('#save_button').on('click', function(event) {
              console.log("New user data: ", new_data);
              update_data(new_data);
              event.preventDefault();
            });
            
          } else {
            //show message
            $('#ajax_message').addClass('alert-new-warning').html(response.data);
          }
        },
        complete: function() {
          $('span#small_loader').fadeOut('fast');
        },
        error: function(jqXHR, textStatus){
           console.log("Status: ", jqXHR.status);
           console.log("textStatus: ", textStatus);
        }
      });
    }
    
    e.preventDefault();
  });
});

function is_valid_email_address(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};

function update_data(data) {
  jQuery.ajax({
    type: "POST",
    url: main_ajax.ajaxurl,
    data:data,
    dataType:"json",
    beforeSend: function() {
      $('span#success_loader').fadeIn();
    },
    success: function(response) {
      $('span#success_loader').fadeOut('fast');
      $.pnotify({
        title: "Të dhënat u shtuan me sukses",
        text: "<br /><p>Pasi të miratohet, shpallja do të jetë në faqen tonë.</p>",
        styling: 'bootstrap',
        icon:"icon-ok icon-2x",
        closer_hover:true,
        nonblock: false,
        nonblock_opacity: 2,
        sticker: false,
        delay:7000,
        hide:true,
        history: false,
        animation:"fade",
        animate_speed: "fast",
        type:"success",
        width:'500px',
        remove:true,
        before_open: function(pnotify) {
        pnotify.css({
                "top": ($(window).height() / 2) - (pnotify.height() / 2),
                "left": ($(window).width() / 2) - (pnotify.width() / 2)
            });
        }
      });
      //console.log("Response: ", response);
      setTimeout(function() {
        window.location.reload();
      }, 3000);
    },
    error: function(jqXHR, textStatus){
      $.pnotify({
        title: "Kanë ndodhur gabime !",
        text: "Të dhënat nuk janë ndryshuar.",
        styling: 'bootstrap',
        closer_hover:true,
        nonblock: false,
        nonblock_opacity: 2,
        sticker: false,
        delay:6000,
        hide:true,
        history: false,
        animation:"fade",
        animate_speed: "fast",
        type:"error",
        width:'500px',
        remove:true,
        before_open: function(pnotify) {
        pnotify.css({
                    "top": ($(window).height() / 2) - (pnotify.height() / 2),
                    "left": ($(window).width() / 2) - (pnotify.width() / 2)
                });
            }
        });
      //console.log("Status: ", jqXHR.status);
      //console.log("textStatus: ", textStatus);
    }
  });
}