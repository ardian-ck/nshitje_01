<?php
function ac_add_meta_boxes() {
    add_meta_box(
        'ac_meta_box',//$id
        'Të dhënat për automjetin:',
        'ac_show_custom_metaboxes',
        'automjete',
        'normal',
        'high'
    );
}
add_action('add_meta_boxes', 'ac_add_meta_boxes');

if(is_admin()) {
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('jquery-ui-slider');
    wp_enqueue_script('custom-js', get_template_directory_uri().'/js/custom.js');
    wp_enqueue_style('jquery-ui-custom', get_template_directory_uri().'/css/jquery-ui-custom.css');
}

$prefix = "ac_";
$custom_meta_fields = array(
    array(
        'label' => 'Lloji i automjetit',
        'desc' => 'Llojet e automjeteve, psh. <em>vetura</em>, <em>motoçikleta</em>, <em>kamionë</em> etj.',
        'id' => 'lloji',
        'type' => 'select_lloji'
    ),
    array(
        'label' => 'Tipi i automjetit',
        'desc' => 'Paraqet tipin e automjetit, psh. <em>Volkswagen</em>, <em>Mercedes</em> etj.',
        'id' => 'tipi',
        'type' => 'text_tipi'
    ),
    array(
        'label' => 'Modeli i automjetit',
        'desc' => 'Paraqet modelin e automjetit, për një tip të caktuar, psh. Audi <em>A4</em>, BMW <em>X5</em>.',
        'id' => 'modeli',
        'type' => 'text_modeli'
    ),
    array(
        'label' => 'Madhesia e motorit',
        'desc' => 'Madhesia e motorit(cc). Vërejtje: Përdoret vetëm te motoçikletat.',
        'id' => 'madhesia_motorit',
        'type' => 'text_madhesia_motorit'
    ),
    array(
        'label' => 'Viti i prodhimit: ',
        'desc' => 'Viti kur automjeti është prodhuar.',
        'id' => $prefix . 'viti_prodhimit',
        'type' => 'text_viti'
    ),
    array(
        'label' => 'Lënda djegëse',
        'desc' => 'Lënda djegëse e automjetit',
        'id' => 'lenda_djegese',
        'type' => 'select_lenda'
    ),
    array(
        'label' => 'Tipi i transmisionit',
        'desc' => 'Tipi i transmisionit të automjetit',
        'id' => 'transmisioni',
        'type' => 'select_tipi_trans'
    ),
    array(
        'label' => 'Kubikazhi',
        'desc' => 'Kubikazha e automjetit',
        'id' => $prefix . 'kubikazhi',
        'type' => 'select_kubikazhi',
        'options' => array(
            'one' => array(
                'label' => '50',
                'value' => '50'
            ),
            'two' => array(
                'label' => '125',
                'value' => '125'
            ),
            'three' => array(
                'label' => '250',
                'value' => '250'
            ),
            'four' => array(
                'label' => '400',
                'value' => '400'
            ),
            'five' => array(
                'label' => '600',
                'value' => '600'
            ),
            'six' => array(
                'label' => '900',
                'value' => '900'
            ),
            'seven' => array(
                'label' => '1000',
                'value' => '1000'
            ),
            'eight' => array(
                'label' => '1100',
                'value' => '1100'
            ),
            'nine' => array(
                'label' => '1200',
                'value' => '1200'
            ),
            'ten' => array(
                'label' => '1300',
                'value' => '1300'
            ),
            'eleven' => array(
                'label' => '1400',
                'value' => '1400'
            ),
            'twelve' => array(
                'label' => '1500',
                'value' => '1500'
            ),
            'thirteen' => array(
                'label' => '1600',
                'value' => '1600'
            ),
            'fourteen' => array(
                'label' => '1700',
                'value' => '1700'
            ),
            'fifteen' => array(
                'label' => '1800',
                'value' => '1800'
            ),
            'sixteen' => array(
                'label' => '1900',
                'value' => '1900'
            ),
            'seventeen' => array(
                'label' => '2000',
                'value' => '2000'
            ),
            'eighteen' => array(
                'label' => '2100',
                'value' => '2100'
            ),
            'nineteen' => array(
                'label' => '2200',
                'value' => '2200'
            ),
            'twenty' => array(
                'label' => '2300',
                'value' => '2300'
            ),
            'twentyone' => array(
                'label' => '2400',
                'value' => '2400'
            ),
            'twentytwo' => array(
                'label' => '2500',
                'value' => '2500'
            ),
            
            'twentythree' => array(
                'label' => '2600',
                'value' => '2600'
            ),
            'twentyfour' => array(
                'label' => '2700',
                'value' => '2700'
            ),
            'twentyfive' => array(
                'label' => '2800',
                'value' => '2800'
            ),
             'twentysix' => array(
                'label' => '2900',
                'value' => '2900'
            ),
            'twenyseven' => array(
                'label' => '3000',
                'value' => '3000'
            ),
            'twentyeight' => array(
                'label' => '3100',
                'value' => '3100'
            ),
            'twentynine' => array(
                'label' => '3500',
                'value' => '3500'
            ),
            'thirty' => array(
                'label' => '4000',
                'value' => '4000'
            ),
            'thirtyone' => array(
                'label' => 'Më shumë se 4000',
                'value' => '4000+'
            )
        )
    ),
    array(
        'label' => 'Lokacioni',
        'desc' => 'Lokacioni i automjetit',
        'id' => 'lokacioni',
        'type' => 'select_lokacioni'
    ),
    array(
        'label' => 'Kilometrazhi: ',
        'desc' => 'Kilometra të kaluara',
        'id' => $prefix . 'km_kaluara',
        'type' => 'text_km'
    ),
    array(
        'label' => 'Çmimi i automjetit në euro',
        'desc' => ' Vërejtje: Shkruaj vetem numër, pa presje apo hapesire !',
        'id' => $prefix . 'cmimi_auto',
        'type' => 'text_cmimi'
    ),
    array(
        'label' => 'Çmimi i vjetër',
        'desc' => ' Vërejtje: Çmimi para ndryshimit !',
        'id' => $prefix . 'cmimi_ndryshuar_auto',
        'type' => 'text_cmimi_ndryshuar'
    ),
    array(
        'label' => 'Çmimi i diskutueshëm / fiks.',
        'desc' => 'Kjo fushe tregon nëse çmimi i shënuar është përfundimtar apo mund të diskutohet.',
        'id' => $prefix . 'cmimi_test',
        'type' => 'checkbox_cmimi',
        'options' => array(
            'one' => array(
                'label' => 'Me marrëveshje',
                'value' => '0'
            ),
            'two' => array(
                'label' => 'Fiks',
                'value' => '1'
            ),
            'three' => array(
                'label' => 'Me mundësi ndërrimi',
                'value' => '2'
            )
        )
    ),
    array(
        'label' => 'Foto 1:',
        'desc' => 'Fotot e automjetit',
        'id' => $prefix . 'foto1',
        'type' => 'image'
    ),
    array(
        'label' => 'Foto 2:',
        'desc' => 'Foto tjeter e automjetit',
        'id' => $prefix.'foto2',
        'type' => 'image1'
    ),
    array(
        'label' => 'Foto 3:',
        'desc' => 'Foto tjeter e automjetit',
        'id' => $prefix.'foto3',
        'type' => 'image2'
    ),
    array(
        'label' => 'Foto 4:',
        'desc' => 'Foto tjeter e automjetit',
        'id' => $prefix.'foto4',
        'type' => 'image3'
    ),
    array(
        'label' => 'Emri i postuesit',
        'desc' => 'Emri i postuesit të shpalljes.',
        'id' => $prefix.'emri_postuesit',
        'type' => 'text_emri_post'
    ),
    array(
        'label' => 'Email e postuesit',
        'desc' => 'Email e postuesit të shpalljes.',
        'id' => $prefix.'email_postuesit',
        'type' => 'text_email_post'
    ),
    array(
        'label' => 'Numri i telefonit',
        'desc' => 'Numri kontaktues i postuesit të shpalljes',
        'id' => $prefix.'nr_postuesit',
        'type' => 'text_nr_post'
    ),
    array(
        'label' => 'Data',
        'desc' => 'Data e publikimi të shpalljes.',
        'id' => $prefix.'data_publikimit',
        'type' => 'text_data'
    ),
    array(
        'label' => 'ID',
        'desc' => 'ID për identifikimin e postit',
        'id' => $prefix.'timestamp',
        'type' => 'text_timestamp'
    ),
    array(
        'label' => 'Ndryshimet në post. (0 deri ne 3)',
        'desc' => 'Numri që tregon sa herë është ndryshuar posti nga shfrytëzuesi.',
        'id' => $prefix.'nr_ndryshimeve',
        'type' => 'text_ndryshime'
    ),
    array(
        'label' => 'Shpallja në faqen kryesore',
        'desc' => 'Kjo fushe mundeson qe shpallje te qendroje ne sliderin e faqes kryesore dhe ne listen e shpalljeve te vequara.',
        'id' => $prefix.'speciale',
        'type' => 'radio_special',
        'options' => array(
            'one' => array(
                'label' => 'Shpallja është në faqen kryesore.',
                'value' => '1'
            ),
            'two' => array(
                'label' => 'Shpallja nuk eshte ne faqen kryesore.',
                'value' => '0'
            )
        )
    ),
    array(
        'label' => 'Shpallja valide',
        'desc' => 'Kjo fushë mund të ndryshohet në mënyrë dinamike përmes email-it të postuesit, i cili në rastin e shitjes së automjetit, konfirmon email-in të cilin e kemi dërguar në fillim.
                   Në këtë rast, statusi i postit ndryshon në, draft ose deleted.',
        'id' => $prefix.'valide',
        'type' => 'radio_valide',
        'options' => array(
            'one' => array(
                'label' => 'Shpallja është valide',
                'value' => '1'
            ),
            'two' => array(
                'label' => 'Shpallja ka skaduar ose produkti është shitur.',
                'value' => '0'
            )
        )
    ),
    array(
        'label' => 'Kodi për ndryshime.',
        'desc'  => 'Ky kodi i dërgohet shfrytezuesit për të ndryshuar postimin.',
        'id'    => $prefix.'kodi',
        'type'  => 'text_kodi'
    )
);


function ac_show_custom_metaboxes() {
    global $custom_meta_fields, $post;

    echo '<input type="hidden" name="ac_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
    echo '<table class="form-table">';
    //var_dump($post);
    echo '<br /><br />';
    foreach($custom_meta_fields as $field) {
        $meta = get_post_meta($post->ID, $field['id'], true);
        //var_dump($meta);
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
                <td>';

        switch($field['type']) {
            case 'select_lloji':
                echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
                <option value=""> -- zgjedh llojin -- </option>';

                $terms = get_terms($field['id'], 'get=all');;
                $selected = wp_get_object_terms($post->ID, $field['id']);
                print_r($selected);
                foreach($terms as $term) {
                    if(!empty($selected) && !strcmp($term->slug, $selected[0]->slug))
                        echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>';
                    else 
                        echo '<option value="'.$term->slug.'">'.$term->name.'</option>';
                }

                $taxonomy = get_taxonomy($field['id']);
                echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">
                Menagjo '.$taxonomy->label.'</a></span>';
            break;

            case 'text_tipi':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="50" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_modeli':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="50" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_madhesia_motorit':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="50" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_viti':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="50" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'select_lenda':
                echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
                <option value=""> -- zgjedh lënden djegëse -- </option>';

                $terms = get_terms($field['id'], 'get=all');
                $selected = wp_get_object_terms($post->ID, $field['id']);

                foreach($terms as $term) {
                    if(!empty($selected) && !strcmp($term->slug, $selected[0]->slug))
                        echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>';
                    else 
                        echo '<option value="'.$term->slug.'">'.$term->name.'</option>';
                }

                $taxonomy = get_taxonomy($field['id']);
                echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">
                Menagjo '.$taxonomy->label.'</a></span>';
            break;  

            case 'select_tipi_trans':
                echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
                <option value=""> -- zgjedh tipin e transmisionit -- </option>';

                $terms = get_terms($field['id'], 'get=all');
                $selected = wp_get_object_terms($post->ID, $field['id']);

                foreach($terms as $term) {
                    if(!empty($selected) && !strcmp($term->slug, $selected[0]->slug))
                        echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>';
                    else 
                        echo '<option value="'.$term->slug.'">'.$term->name.'</option>';
                }

                $taxonomy = get_taxonomy($field['id']);
                echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">
                Menagjo '.$taxonomy->label.'</a></span>';
            break;

            case 'select_kubikazhi':
                echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';
                foreach($field['options'] as $option) {
                    echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
                }
                echo '</select><br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'select_lokacioni':
                echo '<select name="'.$field['id'].'" id="'.$field['id'].'">
                <option value=""> -- zgjedh lokacionin -- </option>';

                $terms = get_terms($field['id'], 'get=all');
                $selected = wp_get_object_terms($post->ID, $field['id']);

                foreach($terms as $term) {
                    if(!empty($selected) && !strcmp($term->slug, $selected[0]->slug))
                        echo '<option value="'.$term->slug.'" selected="selected">'.$term->name.'</option>';
                    else 
                        echo '<option value="'.$term->slug.'">'.$term->name.'</option>';
                }

                $taxonomy = get_taxonomy($field['id']);
                echo '</select><br /><span class="description"><a href="'.get_bloginfo('home').'/wp-admin/edit-tags.php?taxonomy='.$field['id'].'">
                Menagjo '.$taxonomy->label.'</a></span>';
            break;

            case 'text_km':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_cmimi':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_cmimi_ndryshuar':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'checkbox_cmimi':
                foreach($field['options'] as $option) {
                    //print_r($option['value']);
                    print_r($meta);
                    
                    echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['id'].'[]" id="'.$option['value'].'"'.($meta && in_array($option['value'], (array) $meta) ? ' checked="checked"' : '').' /> 
                                    <label for="'.$option['value'].'">'.$option['label'].'</label><br />';
                }
                echo '<span class="description">'.$field['desc'].'</span>';
            break;

            case 'image':
                $image = get_template_directory_uri().'/images/default.png';
                echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
                if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0];
                var_dump($image);
                }               
                echo    '<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
                        <img src="'.$image.'" class="custom_preview_image" alt="" /><br />
                        <input class="custom_upload_image_button button" type="button" value="Shto Foto" data-cp-post-id="'.$post->ID.'" />
                        <small>&nbsp;<a href="#" class="custom_clear_image_button">Fshij Foto</a></small>
                        <br clear="all" /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'image1':
                $image = get_template_directory_uri().'/images/default.png';
                $default = get_template_directory_uri().'/images/default.png';

                echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
                if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0]; 
                var_dump($image);
                }
                isset($image) ? $image : $image = $default;     
                echo    '<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
                            <img src="'.$image.'" class="custom_preview_image" alt="" /><br />
                                <input class="custom_upload_image_button button" type="button" value="Shto Foto" />
                                <small>&nbsp;<a href="#" class="custom_clear_image_button">Fshij Foto</a></small>
                                <br clear="all" /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'image2':
                $image = get_template_directory_uri().'/images/default.png';
                echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
                if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0]; 
                var_dump($image);
                }               
                echo    '<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
                            <img src="'.$image.'" class="custom_preview_image" alt="" /><br />
                                <input class="custom_upload_image_button button" type="button" value="Shto Foto" />
                                <small>&nbsp;<a href="#" class="custom_clear_image_button">Fshij Foto</a></small>
                                <br clear="all" /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'image3':
                $image = get_template_directory_uri().'/images/default.png';
                echo '<span class="custom_default_image" style="display:none">'.$image.'</span>';
                if ($meta) { $image = wp_get_attachment_image_src($meta, 'medium'); $image = $image[0]; 
                var_dump($image);
                }               
                echo    '<input name="'.$field['id'].'" type="hidden" class="custom_upload_image" value="'.$meta.'" />
                            <img src="'.$image.'" class="custom_preview_image" alt="" /><br />
                                <input class="custom_upload_image_button button" type="button" value="Shto Foto" />
                                <small>&nbsp;<a href="#" class="custom_clear_image_button">Fshij Foto</a></small>
                                <br clear="all" /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_emri_post':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_email_post':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_nr_post':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_data':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_timestamp':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="32" maxlength="32"/>
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_ndryshime':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="1" maxlength="1"/>
                <br /><span class="description">'.$field['desc'].'</span>';
            break;

            case 'text_kodi':
                echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" />
                <br /><span class="description">'.$field['desc'].'</span>';
            break;
		
	    case 'radio_special':
                    foreach($field['options'] as $option) {
                        echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$meta == $option['value'] ? ' checked="checked"' : '',' />
                            <label for="'.$option['value'].'">'.$option['label'].'</label><br />';
                    }
                    echo '<span class="description">'.$field['desc'].'</span>';

            break;
            
            case 'radio_valide':
                foreach($field['options'] as $option) {
                    echo '<input type="radio" name="'.$field['id'].'" id="'.$option['value'].'" value="'.$option['value'].'" ',$meta == $option['value'] ? ' checked="checked"' : '',' />
                        <label for="'.$option['value'].'">'.$option['label'].'</label><br />';
                }
                echo '<span class="description">'.$field['desc'].'</span>';
            break;

        }
        echo '</td></tr>';
    }
    echo '</table>';
}



function ac_remove_taxonomy_boxes() {
    remove_meta_box('categorydiv', 'post', 'side');
}
add_action('admin_menu', 'ac_remove_taxonomy_boxes');

function ac_save_custom_meta($post_id) {
    global $custom_meta_fields;

    //verifikojme nonce.
    if(!wp_verify_nonce($_POST['ac_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
    
    //testojm per autosave
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    //testojm per leje. A mundemi te ndryshojm faqen / postin.
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }

    foreach ($custom_meta_fields as $field) {

        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            //fshijme te dhenat e postit por jo attachment - fotot.Prandaj duhet nje funksion tjeter.
            delete_post_meta($post_id, $field['id'], $old);
            //fshijm fajllat te cilet kane mbetur pasi posti eshte fshire.
            ac_remove_unattached_images($post_id);
        }
    }
    
    $post = get_post($post_id);
    $category = $_POST['category'];
    wp_set_object_terms( $post_id, $category, 'category' );
}

add_action('save_post', 'ac_save_custom_meta');

if(!class_exists('SH_Walker_TaxonomyDropdown')) {
   class SH_Walker_TaxonomyDropdown extends Walker_CategoryDropdown {
       function start_el(&$output, $category, $depth = 0, $args = array(), $id = 0) {
           $pad = str_repeat('&nbsp', $depth * 3);
           $cat_name = apply_filters('lists_cats', $category->name, $category);
           
           if(!isset($args['value'])) {
               $args['value'] = ($category->taxonomy != 'category' ? 'slug' : 'id');
           }
           
           $value = ($args['value'] == 'slug' ? $category->slug : $category->term_id);
           
           $output .= "\t<option class=\"level-depth\" value=\"".$value."\"";
           if($value == (string) $args['selected']){
               $output .= ' selected="selected"';
           }
           
           $output .= '>';
           $output .= $pad.$cat_name;
           
           if($args['show_count'])
               $output .= '&nbsp;&nbsp;('.$category->count.')';
           $output .= '</option>\n';
       }
   }
}
?>